/start category/
agrs;

/rules/

/verbs/
soma :: =dobj =d v;
soma :: =dobj =d =adv v;
nunua :: =dobj =d v;
nunua :: =dobj =d =adv v;
ona :: =dobj =d v;
ona :: =dobj =d =adv v;
la :: =dobj =d v;
la :: =dobj =d =adv v;

/adverbs/
:: =p adv;
:: =p adv -adv;

:: p; / p was not defined initially /

/nouns/
gazeti :: n5;
kitabu :: n7;
kitunguu :: n7;

/determiner phrases/
:: d -subj -fpssubj;
:: d -subj -spssubj;
:: =n5 dobj -n5obj -obj;
:: =n7 dobj -n7obj -obj;
:: dobj -fpsobj -obj;

/clitics/
:: =v +n5obj cl -cl;
li :: =v +n5obj cl -cl;
:: =v +n7obj cl -cl;
ki :: =v +n7obj cl -cl;
ni :: =v +fpsobj cl -cl;

/vp caps/
:: =cl +adv w;
:: =cl w;
:: =w +obj x;
:: =x +subj y;
:: =y +cl z;

/ip region/
li :: =z aux;
na :: =z aux;
a :: =z aux;
ka :: =z aux;

ni :: =aux +fpssubj agrs;
u :: =aux +spssubj agrs;

