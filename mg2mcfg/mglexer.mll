{
(** Returns a stream of lexemes defined in mglexer.mll
	@author Matthieu Guillaumin
	@see 'mglexer.mll' for lexemes used *)
open Mgparser;;
exception LexingError of int*string
}

rule token = parse
 [' ' '\t' '\n' '\r'] {token lexbuf}
| ( '/' [^'/']* '/')   {token lexbuf}      (* comments are between '/'s *)
| ( '%' [^'\n']* '\n' )   {token lexbuf}   (* or behind a % *)
| ';' {ENDRULE}
| "::" {DEFINE}
| '=' {EQUAL}
| '+' {PLUS}
| '-' {MINUS}
| (['a'-'z'] | ['A'-'Z'] | ['0'-'9'])+ {IDENT(Lexing.lexeme lexbuf)}
| ('\"' ([^'\"']* as idiom) '\"') {IDENT(idiom)}
| eof {ENDGRAMMAR}
| (_) {raise(LexingError(Lexing.lexeme_start lexbuf,Lexing.lexeme lexbuf))}
