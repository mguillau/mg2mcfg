%{
(** Parses the stream returned by the Mglexer lexer according to the grammar of MGs defined in mgparser.mly.
    Please note that your grammar should start with a line defining its start symbols.
	@author Matthieu Guillaumin
	@see 'mgparser.mly' for the explicit accepted grammar *)
open Mgtransform.Feature;;
open Mgtransform.Rule;;
open Mgverify;;

%}

%token ENDRULE ENDGRAMMAR
%token DEFINE
%token EQUAL PLUS MINUS
%token EMPTY

%token <string> IDENT

%start mg
%type <string list * Mgtransform.Rule.grammar> mg

%%

mg:
  startlist lexrulelist ENDGRAMMAR {check($1,$2)};

startlist:
  IDENT slist ENDRULE {$1::$2};

slist:
 {[]}
| IDENT slist {$1::$2};

lexrulelist:
 {[]}
| lexrule lexrulelist {$1::$2};

lexrule:
 vocab sign featurelist category licenseelist ENDRULE { {voc=$1; nature=$2; features=($3@[$4]@$5)} };

sign:
  DEFINE {LEXICAL}
;

vocab:
  {""}
| IDENT {$1};

featurelist:
 {[]}
| feat featurelist {$1::$2};

feat:
 featmod IDENT { {name=$2; modifier=$1} };

featmod:
  EQUAL {SELECTOR}
| PLUS  {LICENSOR}

category:
  IDENT { {name=$1; modifier=CATEGORY} };

licenseelist:
  {[]}
| licensee licenseelist {$1::$2};

licensee:
| MINUS IDENT { {name=$2; modifier=LICENSEE} };
%%
