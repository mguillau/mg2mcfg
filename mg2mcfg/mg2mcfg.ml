(** Defines the functions to treat the grammar depending on the wished input and output
@author Matthieu Guillaumin *)

open Mgtransform;;
open Mglexer;;
open Mgparser;;

module EndUser =
 struct

 exception No_Specified_Input
 exception Multiple_Input_Specified

 (** @return the grammar given in the given channel *)
 let load_gram_from_channel in_ch =
    let lexbuf = Lexing.from_channel in_ch in (mg token lexbuf)

 (** @return the grammar from standard input *)
 let load_gram_from_stdin() = load_gram_from_channel stdin

 (** @return the grammar specified in the file of given filename *)
 let load_gram_from_file s = load_gram_from_channel (open_in s)

 (** @return the grammar from the given string *)
 let load_gram_from_string s = let lexbuf = Lexing.from_string s in (mg token lexbuf)

 (** Outputs onto the given channel *)
 let process_gram_to_channel out_ch (s,g) dict =
    let ht = new aRuleChart (s,List.length g) in
    ht#acquire_grammar g;
    ht#compute();
    ht#print_mcfg out_ch;
    if dict <> "" then (ht#dict (open_out dict)) 

 (** Outputs on standard output *)
 let process_gram_to_stdout = process_gram_to_channel stdout

 (** Outputs into the given file (overwrite mode) *)
 let process_gram_to_file s = process_gram_to_channel (open_out s)
end;;

(** The main function. *)
let _ = 
 let usage_msg = "Usage: "^(Sys.argv.(0))^" [<filename>|-i] [-o <output>] [-dict <file>]"  in
 let output = ref "" and input = ref "" and input_stdin = ref false and dict = ref "" in 
 let specs = [("-i",Arg.Set input_stdin,  "     Set input to standard input instead of file <filename>");
              ("-o",Arg.Set_string output,"     Set output file to <output> (default: standard output)");
	      ("-dict",Arg.Set_string dict,  "  Output translation of mcfg symbols to file <file>")  ]
 in let an_fun x = input:=x in
 try
  Arg.parse specs an_fun usage_msg;
  let sg = 
  match !input, !input_stdin with
   | "",true -> prerr_string "/*** Press Ctrl-D when you are finished ***/\n";
		flush stderr;
		EndUser.load_gram_from_stdin()
   | "", false -> raise EndUser.No_Specified_Input
   | _, true -> raise EndUser.Multiple_Input_Specified
   | s, false -> EndUser.load_gram_from_file s
  in
  match !output with
   | "" -> prerr_string "\n/*** This is the corresponding MCFG ***/\n";
	   flush stderr;
           EndUser.process_gram_to_stdout sg !dict
   | s -> EndUser.process_gram_to_file s sg !dict
 with
  | EndUser.No_Specified_Input -> Arg.usage specs usage_msg
  | EndUser.Multiple_Input_Specified -> Arg.usage specs usage_msg
  | Mglexer.LexingError(i,s) -> prerr_string ("There is an error with your grammar at character "^(string_of_int i)^" with the string \""); prerr_string s; prerr_string "\".\n"
  | Parsing.Parse_error -> prerr_string "Parsing Error!\nYour grammar is not well formed: please refer to the documentation\n"
  | Failure s -> prerr_string "An error has occured: \""; prerr_string s; prerr_string "\"\nYou should check your grammar for mispelling\n and make sure you use only alphabetic characters for vocabulary and features.\n"
 ;;
