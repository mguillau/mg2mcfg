(** Defines all the data structures and functions to convert MGs to MCFGs
  @author Matthieu Guillaumin
*)

(** The string dictionary *)
module AssocStrings = Dict.Make(struct type t = string
                                 let compare = Pervasives.compare
                                 let print out_ch _ = output_string out_ch end);;

(** Sets of integers *)
module IntSet = Set.Make(struct type t=int let compare x y = x-y end);;

(** Mcfg rules, and printing functions *)
module Mcfg =
struct
 type tuple = int
 type length = int
 type mode = bool
 type index = int

 type left = Start
        | Lexical of tuple * int * int
        | NonTerminal of tuple

 type right = LexTerminal of string
            | LexNode of tuple * int * int * int
            | LexRoot of tuple * int * int
	    | Simple of tuple
            | Merge1or2 of tuple * length * tuple * length * mode
            | Merge3 of tuple * length * tuple * length
            | Move of tuple * length * index * mode

 type t = left * right

 let compare = Pervasives.compare

 let print_left out_ch l = 
   let pr = output_string out_ch
   and pri i = output_string out_ch (string_of_int i) in
   let prt() = pr "t" and pru() = pr "_" in
     (match l with
	| Start -> pr "S"
	| NonTerminal i -> prt(); pri i
	| Lexical (i,j,k) -> prt(); pri i; pru(); pri j; pru(); pri k)

 let print_right out_ch r =
   let pr = output_string out_ch
   and pri i = output_string out_ch (string_of_int i) in
   let prt() = pr "t" and prs() = pr " " and pru() = pr "_" in
   let rec rpr a d n = if d<n+1 then ( pr "["; pri a; pr ","; pri d; pr "]"; rpr a (d+1) n ) in
   let rec rpr2 i d n = match d with 
     | d when d>n -> ()
     | d when d=0 -> pr "[0,"; pri i; pr ";0,0]"; rpr2 i 1 n
     | d when d=i -> rpr2 i (d+1) n
     | d -> pr "[0,"; pri d; pr "]"; rpr2 i (d+1) n in
   (match r with
	| LexTerminal s -> pr "\""; pr s; pr "\""
	| LexNode (i,j,k1,k2) -> prt(); pri i; pru(); pri j; pru(); pri k1; prs(); prt();
	    pri i; pru(); pri j; pru(); pri k2; prs(); pr "[0,0;1,0]"
	| LexRoot (i,j,k) -> prt(); pri i; pru(); pri j; pru(); pri k; prs(); pr "[0,0]"
	| Simple n -> prt(); pri n; prs(); pr "[0,0]"
        | Merge1or2 (t1,l1,t2,l2,m) -> prt(); pri t1; prs(); prt(); pri t2; prs();
                                       (match m with (* m = is_lexical *)
                                        | true -> pr "[0,0;1,0]"
                                        | false -> pr "[1,0;0,0]");
                                       rpr 0 1 l1; rpr 1 1 l2; if m then pr " (* merge1 *)" else pr " (* merge2 *)"
        | Merge3 (t1,l1,t2,l2) -> prt(); pri t1; prs(); prt(); pri t2; prs(); rpr 0 0 l1; rpr 1 0 l2; pr " (* merge3 *)"
        | Move (t1,l1,i,b) -> prt(); pri t1; prs(); if b then (rpr2 i 0 l1) else (rpr 0 0 l1); pr " (* move *)" )

 (** Prints a mcfg rule, generating the map function in the process *)
 let print out_ch (l,r) =
   print_left out_ch l;
   output_string out_ch " --> ";
   print_right out_ch r;
   output_string out_ch "\n"

 (** Calculates the left symbols corresponding to the symbols on the right side of the mcfg rule *)
 let following (_,r) = match r with
   | LexTerminal _ -> []
   | LexNode (i,j,k1,k2) -> [ Lexical(i,j,k1); Lexical(i,j,k2) ]
   | LexRoot (i,j,k) -> [ Lexical(i,j,k) ]
   | Simple t -> [ NonTerminal t ]
   | Merge1or2 (t1,_,t2,_,_) -> [ NonTerminal t1; NonTerminal t2 ]
   | Merge3 (t1,_,t2,_) -> [ NonTerminal t1; NonTerminal t2 ] 
   | Move (t,_,_,_) -> [ NonTerminal t ]

end;;   

module AssocLeftMcfg = Dict.Make(
struct
  type t = Mcfg.left
  let compare = Pervasives.compare
  let print out_ch g = Mcfg.print_left out_ch
end);;
module AssocRightMcfg = Dict.Make(
struct
  type t = Mcfg.right
  let compare = Pervasives.compare
  let print out_ch g = Mcfg.print_right out_ch
end);;





(** Defines the type and basic functions for features *)
module Feature =
struct
  (** The different types of features *)
  type ftype = CATEGORY | SELECTOR | LICENSOR | LICENSEE
      
  (** The type of features: a record of a name and a sort of feature *)
  type t = { name: string; modifier: ftype }

  let compare = Pervasives.compare

  (** @return [true] iff the feature is of type [CATEGORY] *)
  let is_cat x = x.modifier = CATEGORY
				
  (** @return [true] iff the feature is of type [SELECTOR] *)
  let is_sel x = x.modifier = SELECTOR
				
  (** @return [true] iff the feature is of type [LICENSOR] *)
  let is_lic x = x.modifier = LICENSOR
				
  (** @return [true] iff the feature is of type [LICENSEE] *)
  let is_lee x = x.modifier = LICENSEE
				
  (** @return [true] iff the two features have the same name *)
  let have_same_name x y = x.name = y.name
				      
  (** @return [true] iff the feature has the given name *)
  let compare_name x n = x.name = n

  (** @return the name of the feature *)
  let name f = f.name
		 
  (** Pretty-prints a feature *)
  let print out_ch f = 
    let pr = output_string out_ch in
    let aux = function SELECTOR -> pr " ="
      | LICENSOR -> pr " +"
      | LICENSEE -> pr " -"
      | _ -> pr " "
    in aux f.modifier; pr f.name
	
  (** Pretty-prints a list of features *)
  let rec print_list out_ch = function
      [] -> ()
    | f::q -> print out_ch f; print_list out_ch q
	
  (** @return the string pretty-printing a feature *)
  let to_string f =
    let aux = function SELECTOR -> " ="
      | LICENSOR -> " +"
      | LICENSEE -> " -"
      | _ -> " "
    in (aux f.modifier)^(f.name)
	 
  (** @return the string pretty-printing a list of featuers *)
  let rec list_to_string = function
      [] -> ""
    | f::q -> (to_string f)^(list_to_string q)
	
end;;





(** The module of rules as defined by the input grammar *)
module Rule =
struct
  exception Non_Compatible_Tuples
  exception Not_Unique_Licensee of string
  exception No_Licensee of string
    
  (** The different types of rules: simple ([LEXICAL]) or complex ([DERIVED]) *)
  type rtype = LEXICAL | DERIVED
      
  (** The type of rules: a left string element, a ruletype and a list of features *)
  type t = { voc: string; nature: rtype; features: Feature.t list }
      
  (** Simply a list of rules *)
  type grammar = t list
      
  (** A definition of tuples or rules *)
  type tuple = t * (t list)

  (** Determines if a rule is an idiom *)
  let is_idiom r = 
    try
     let i = String.index r.voc ' ' in i >= 0
    with Not_found -> false

  (** @return the list of words of the idiom *)
  let split_idiom r =
    let rec aux buf index words str = match index,str.[index] with
      | 0,' ' -> (if buf <> "" then [buf] else [])@words
      | 0,c -> (Char.escaped c ^ buf)::words 
      | _,' ' -> aux "" (index -1) ((if buf<>"" then [buf] else [])@words) str
      | _,c -> aux (Char.escaped c ^ buf) (index -1) words str
    in aux "" (String.length r.voc -1) [] r.voc
      
  let to_string a = (a.voc)^" :"^(match a.nature with LEXICAL -> ":" | _ -> "")^(Feature.list_to_string a.features)

  let makemcfg n x = (Mcfg.NonTerminal n,Mcfg.LexTerminal (x.voc))

  (** @return the list of MCFG rules to generate the given idiom, numbered as the given integer *)
  let gen_idiom_mcfg n p r =
   let l = split_idiom r in
   let itemize m s = (Mcfg.Lexical (n,p,m),Mcfg.LexTerminal s) in
   let rec split = function
    | [] -> [],[]
    | [t] -> [t],[]
    | t1::t2::q -> let l1,l2 = split q in (t1::l1),(t2::l2) in
   let rec map f1 f2 k l = function
    | [],[] -> l,[],k 
    | [t],[] -> l,[t],k
    | (t::q),(t'::q') -> let (a,b,c) = map f1 f2 (k+1) l (q,q') in
                         ((f1 k t t')::a),((f2 k t t')::b),c
    | _ -> raise(Invalid_argument "The list lengths don't match") in
   let f1 k s1 s2 = (Mcfg.Lexical (n,p,k), Mcfg.LexNode (n,p,s1,s2)) in
   let f2 k _ _ = k in
   let rec aux_step k l = function
    | [] -> l
    | [_] -> l
    | next -> let (a,b,c) = map f1 f2 k l (split next) in aux_step c a b   in
   let last m = (Mcfg.NonTerminal n,Mcfg.LexRoot (n,p,m)) in
   let rec aux_init = function
    | 0 -> []
    | n -> n::(aux_init (n-1))
   in
   let z = List.length l in 
   let init = List.rev (aux_init z) in
   let items = List.map2 itemize init l in
   let steps = aux_step (z+1) [] init in
   (last (2*z-1))::(items@steps)
   (* the created balanced binary tree has always 2n-1 internal node, ie steps *)
  
end;;





(** Defines the types and functions for anonymous rules *)
module ARule = 
struct
  
  (** The different types of rules: simple ([LEXICAL]) or complex ([DERIVED]) *)
  type rtype = LEXICAL | DERIVED
      
  (** Conversion form [Rule.rtype] to [ARule.rtype] *)
  let convert_rtype = function
    | Rule.LEXICAL -> LEXICAL
    | Rule.DERIVED -> DERIVED
	
  (** The type of anonymous rules *)
  type t = { nature: rtype; features: Feature.t list}
      
  (** The type for tuples of anonymous rules *)
  type tuple = t * (t list)

  exception No_Unicity
  exception No_Licensee of tuple 
      
  (** Pretty-printing of anonymous rules, looks like the MG input *)
  let print out_ch _ a =
    output_string out_ch (match a.nature with LEXICAL -> "::" | DERIVED -> ":");
    Feature.print_list out_ch a.features
      
  (** @return the string of an anonymous rule, looks like the MG input *)
  let to_string a = (match a.nature with LEXICAL -> "::" | DERIVED -> ":")^(Feature.list_to_string a.features)
		      
  (** @return the anonymous rule corresponding to the given named rule (Rule.rule) *)
  let from_rule (r:Rule.t) = {nature=(convert_rtype r.Rule.nature); features=r.Rule.features}
			       
  (** @return the tuple of anonymous rules corresponding to the given named rule *)
  let tuple_from_rule r = (from_rule r,[])
			    
  (** Pretty-printing of anonymous rules tuples *)
  let print_tuple out_ch f ((r,rl):tuple) = output_string out_ch "("; print out_ch f r;
    let rec aux_print = function
      | [] -> ();
      | t::q -> output_string out_ch ";"; print out_ch f t; aux_print q
    in aux_print rl; output_string out_ch ")"

  let tuple_to_string ((r,rl):tuple) = let rec aux = function
    | [] -> ""
    | t::q -> ";"^to_string t^aux q
  in "("^to_string r^aux rl^")"
	 
  let makestartmcfg n = (Mcfg.Start,Mcfg.Simple n)

  (** @return the first feature of the rule *)
  let first_feature (r,_) = List.hd r.features
			  
  (** Determines if a rule has a licensee feature *)
  let will_move (r,_) =
    let rec aux = function 
	[] -> false
      | t::_ when Feature.is_lee t -> true
      | _::q -> aux q
    in aux r.features

  let is_simple (_,l) = l=[]
	 
  (** @return the tuple resulting from the merge1 or merge2 of [tuple1] and [tuple2],
    and the corresponding MCFG rule. No verifications are made *)
  let merge1or2 tr (((r1,l1):tuple),id1) (((_,l2):tuple),id2) =
    let tuple3 = ({nature=DERIVED; features=(List.tl r1.features)}, l1@l2 ) in
    let id3 = tr tuple3 in
    ((tuple3,id3),(Mcfg.NonTerminal id3,Mcfg.Merge1or2(id1,List.length l1,id2,List.length l2,r1.nature=LEXICAL)))
	
  (** @return the tuple resulting from the merge3 of [tuple1] and [tuple2],
    and the corresponding MCFG rule. No verifications are made *)
  let merge3 tr (((r1,l1):tuple),id1) (((r2,l2):tuple),id2) =
    let tuple3 = ({nature=DERIVED; features=(List.tl r1.features)},
		  l1@[{nature=DERIVED; features=(List.tl r2.features)}]@l2) in
    let id3 = tr tuple3 in
    ((tuple3,id3),(Mcfg.NonTerminal id3,Mcfg.Merge3 (id1,List.length l1,id2,List.length l2)))

  (** Verifies for a tuple beginning with a licensor that there is a licensee and that it is unique,
    and for all tuples that licensees appear at most once
    @raise No_Unicity if the licensee is not found or is found more than once *)
  let check_unicity ((r1,l1):tuple) =
    let first_licensee_list = List.map (Feature.name)
				(List.filter Feature.is_lee
				   (List.fold_right
				      (fun r l -> (List.hd r.features)::l)
				      (l1) [])) in
    let rec aux = function
      | [] -> false
      | [_] -> false
      | t1::t2::q -> (t1=t2)||(aux (t2::q)) in
    let has_duplicates = aux (List.fast_sort Pervasives.compare first_licensee_list) in
      match (Feature.is_lic (first_feature (r1,l1))) with
	| false when has_duplicates -> raise No_Unicity
	| true -> if (has_duplicates ||
		      not(List.mem (Feature.name (first_feature (r1,l1))) first_licensee_list ))
	          then raise No_Unicity
	| _ -> ()
	
  (** @return the tuple resulting from a move inside the given tuple, and the corresponding MCFG rule. No verifications are made
    @raise No_licensee if the tuple is a dead-end *)
  let move tr (((r,l):tuple),id) =
    let split k li = 
      let rec aux lii = function
	  [] -> raise(No_Licensee (r,l))
	| t::q when Feature.name (List.hd t.features) = k && Feature.is_lee (List.hd t.features) -> lii,t,q
	| t::q -> aux (t::lii) q
      in aux [] li
    in
    let (rev_l1,r',l2) = split (Feature.name (List.hd r.features)) l in (* find the corresponding licensee *)
    let b = (List.tl r'.features) = [] in (* is that the final move? *)
    let tuple2 = ({nature=DERIVED; features=(List.tl r.features)},
                  List.rev_append rev_l1 ((if b then [] else [{nature=DERIVED; features=(List.tl r'.features)}])@l2)) in
    let id2 = tr tuple2 in
    ((tuple2,id2),(Mcfg.NonTerminal id2,Mcfg.Move (id,List.length l,List.length rev_l1 + 1,b)))	

end;;


(** The dictionary of tuples *)
module AssocTuple = Dict.Make(
struct
  type t = ARule.tuple
  let compare = Pervasives.compare
  let print = ARule.print_tuple
end);;



(** The class of charts containing anonymous rules *)
class aRuleChart (startsymbols,n) =
object(self) 

  val feat = AssocStrings.empty
  method tr_feat = AssocStrings.find feat
  method rt_feat = AssocStrings.retrieve feat

  val tuples = AssocTuple.empty
  method tr_tuple = AssocTuple.find tuples
  method rt_tuple = AssocTuple.retrieve tuples

  val leftdict = AssocLeftMcfg.empty
  val rightdict = AssocRightMcfg.empty
  val mcfg = Dyn_array.create n
  val printed_mcfg = Dyn_array.create n

  method tr_left = AssocLeftMcfg.find leftdict
  method tr_right = AssocRightMcfg.find	rightdict
  method rt_left = AssocLeftMcfg.retrieve leftdict
  method rt_right = AssocRightMcfg.retrieve rightdict

  (** Add an element to the mcfg set *)
  method add_mcfg (l,r) =
    let l' = self#tr_left l and r' = self#tr_right r
    in Dyn_array.add mcfg l' r'

  (** Notify that a mcfg rule has been printed *)
  method addprintedmcfg (l,r) =
    let l' = self#tr_left l and r' = self#tr_right r
    in Dyn_array.add printed_mcfg l' r'

  (** Returns the list of mcfg rules having its left part inside the given list *)
  method find_mcfg_beginning_inside leftlist =
    let rightlistlist = List.map (function l -> Dyn_array.find mcfg (self#tr_left l)) leftlist
    in let shortlist = List.combine leftlist rightlistlist
    in let rules = List.map (function (l,rl) -> List.map (function r -> (l,self#rt_right r)) rl) shortlist
    in List.concat rules

  (** The hash-table containing tuples with a selector as first feature *)
  val sel = Dyn_array.create n
	      
  (** The hash-table containing fixed categorical tuples *)
  val cat = Dyn_array.create n
	      
  (** The hash-table containing unfixed categorical tuples *)
  val cat_move = Dyn_array.create n
		   
  (**  The hash-table containing tuples with a licensor as first feature *)
  val lic = Dyn_array.create 0

  (** A set used to know which features are concerned by a change *)
  val mutable has_changed = IntSet.empty
		      
  method add_changed x = has_changed <- IntSet.add x has_changed

  (** @return the key of the tuple *)
  method get_key t = self#tr_feat (Feature.name (ARule.first_feature t))
		       
  (** @return the hash-table of the tuple *)
  method where t = let f = ARule.first_feature t in
    match f with
      | f when Feature.is_cat f && not(ARule.will_move t) -> cat
      | f when Feature.is_cat f && ARule.will_move t -> cat_move
      | f when Feature.is_lic f -> lic
      | _ -> sel
	  
  (** Determines if the given tuple is in his hash-table *)
  method mem t = List.mem (self#tr_tuple t) (Dyn_array.find (self#where t) (self#get_key t))
		   
  (** Adds the given tuple into his hash-table
    @return [true] iff the insert has effectively been made (no duplicates in the hash-tables) *)
  method add t =
    let c=not(self#mem t) in if c then (Dyn_array.add (self#where t) (self#get_key t) (self#tr_tuple t)); c; 
      
  method acquire_grammar =  let n=ref 0 in
    List.iter (function x->
                 let b = ARule.tuple_from_rule x in let k = self#tr_tuple b in
		   if Rule.is_idiom x then List.iter (n:= !n+1; self#add_mcfg) (Rule.gen_idiom_mcfg k (!n) (x))
		   else self#add_mcfg (Rule.makemcfg k x);
		   if self#add b then self#add_changed (self#get_key b))
			      
  method find ht k = List.map (fun x -> (self#rt_tuple x),x) (Dyn_array.find ht (self#tr_feat k))
		       
  (** Adds all the starting rules in the MCFG *)
  method close_start_symbols() =
    let f c = List.iter (function (_,k) -> self#add_mcfg (ARule.makestartmcfg k)) (List.filter (function (x,_) -> ARule.is_simple x) (self#find cat c))
    in List.iter f startsymbols
	 
  (** Closes the hash-tables by merge and move operations *)
  method compute() =
    let f1 f ht k ((t1:ARule.tuple),id1) =
	let m = List.map (f self#tr_tuple (t1,id1) ) (self#find ht k) in
	  List.iter (function ((a,_),b) ->
		       try
			 ARule.check_unicity a;
			 let c=self#add a in self#add_mcfg b;
			   if c then (self#add_changed (self#get_key a))
		       with ARule.No_Unicity -> ()
		    ) m
    in
    let f2 _ ((t1:ARule.tuple),id1) = 
      try
	let ((t2,_),b) = ARule.move self#tr_tuple (t1,id1) in let c=self#add t2 in self#add_mcfg b;
	  if c then (self#add_changed (self#get_key t2))
      with ARule.No_Licensee t -> prerr_string ("DEAD TUPLE, move impossible: ");
	ARule.print_tuple stderr (fun x->x) t;
	prerr_string "\n";
	flush stderr
    in
    let f_sel k = let l = self#find sel k in
      List.iter (f1 ARule.merge1or2 cat k) l;
      List.iter (f1 ARule.merge3 cat_move k) l in
    let f_mov k = List.iter (f2 k) (self#find lic k) in
      while not(has_changed = IntSet.empty) do
	let elts = List.map self#rt_feat (IntSet.elements has_changed) in
	  has_changed <- IntSet.empty;
	  List.iter f_sel elts;
	  List.iter f_mov elts;  
      done;
      self#close_start_symbols()
	
  (** Prints out on the given channel the collection of accessible mcfg rules *)
  method print_mcfg out_ch =
    let rec print_aux m =
      let (l,r) = m in let l' = self#tr_left l and r' = self#tr_right r in
      if not(List.mem r' (Dyn_array.find printed_mcfg l')) then
	( Mcfg.print out_ch m;
	  self#addprintedmcfg m;
	  let follow = Mcfg.following m
	  in if follow <> [] then List.iter print_aux (self#find_mcfg_beginning_inside follow))
    in List.iter print_aux (self#find_mcfg_beginning_inside [Mcfg.Start])

  (** Prints the correspondance between mcfg symbols and tuples *)
  method dict out_ch =
    let pr = output_string out_ch in
      AssocTuple.print out_ch self#rt_feat (fun v _ -> List.length (Dyn_array.find printed_mcfg (self#tr_left (Mcfg.NonTerminal v))) > 0) tuples;
      pr "\n"
	
end;;
