(** Defines verification functions for the grammar
@author Matthieu Guillaumin *)
 
(* takes basic_tuples as intput and returns the good tuples *)
open Mtpgtransform.Tuple;;
open Mtpgtransform.Type;;

exception Bad_type of int*int;;
exception No_Atom_Unicity of int;;
exception No_Atom of int*int;;
exception Start_Not_Used of string;;
exception Atom_Not_Used of string;;
exception Not_Defined_Atom of string;;

(** Checks if the typed string is well formed (right inverses, atom, left inverses)
    @raise Bad_type if not *)
let check_well_formed_couple c n p =
 let rec aux bool l1 l2 l3 = function
  | [] -> l1,l2,l3
  | t::q when bool && t.mode = RIGHT -> aux true (t.name::l1) l2 l3 q
  | t::q when bool && t.mode = ATOM  -> aux false l1 [t.name] l3 q
  | t::q when not(bool) && t.mode = LEFT -> aux false l1 l2 (t.name::l3) q
  | _ -> raise(Bad_type (n,p))
 in let (l1,l2,l3) = aux true [] [] [] c.typ in
 if l2 = [] then raise(No_Atom(n,p));
 {left=List.rev l1; right=l3; atom=List.hd l2; value=c.lex};;

(** Checks if the atoms are unique in the tuple
    @raise No_Atom_Unicity if not *)
let check_unicity t n = 
  let l = List.fast_sort Pervasives.compare
          (List.map (function x -> x.atom) t) in
  let rec aux = function
   | [] -> ()
   | [_] -> ()
   | t1::t2::_ when t1=t2 -> raise(No_Atom_Unicity n)
   | _::q -> aux q
  in aux l;;

(** Main checking function. Performs 4 basic checks:
 {ul {li The inverses correspond to an atom}
      {li The right (resp. left) part of a < (resp. >) relation is a start symbol or correspond to an inverse}
      {li The atoms are start symbols or defined inverses or left (resp. right) part of a < (resp. >) relation}
      {li The start symbols are defined atoms or a right (resp. left) part of a < (resp. >) relation}   }
    @return [],[],[] if an error occured; the start symbols, the relations and the tuples otherwise *)
let check (is_not_import,starts,relations,tuple_list) =
  let nda s = Not_Defined_Atom s and anu s = Atom_Not_Used s and snu s = Start_Not_Used s in
  let smaller,bigger = List.split relations in
    
  let rec iter n p = function
    | [] -> []
    | couple::q -> let l'=iter n (p+1) q in (check_well_formed_couple couple n p)::l'
  in let rec iter2 n = function
    | [] -> []
    | couple_list::q -> let l'=iter2 (n+1) q in (iter n 1 couple_list)::l'
  in try
      let l' = iter2 1 tuple_list in
      let rec aux n = function
	| [] -> ()
	| t::q -> check_unicity t n; aux (n+1) q
      in aux 1 l';
	let atoms = List.map (function x -> x.atom) (List.flatten l') in
	let inverses = List.flatten (List.map (function x -> x.left@x.right) (List.flatten l')) in
	let rec error def exp = function
	  | [] -> ()
	  | t::q -> match List.mem t def with
	      | false -> raise(exp t);
	      | _ -> error def exp q
	in
	  if is_not_import then
	    (error (atoms@bigger) nda inverses;
	     error (starts@inverses) anu bigger;
	     error (starts@inverses@smaller) anu atoms;
	     error (atoms@bigger) snu starts;);
	  
	  (starts,relations,l')
	    
    with 
	Bad_type (n,p) -> print_string ("Bad typing in tuple number "^string_of_int n^", coordinate "^string_of_int p^"\n"); [],[],[]
      | No_Atom (n,p) -> print_string ("Tuple number "^string_of_int n^", coordinate "^string_of_int p^" has no atom\n"); [],[],[]
      | No_Atom_Unicity n -> print_string ("Tuple number "^string_of_int n^" has two identical atoms\n"); [],[],[]
      | Start_Not_Used s -> print_string ("Start symbol \""^s^"\" is not used\n"); [],[],[]
      | Not_Defined_Atom s -> print_string ("Atom \""^s^"\" has an inverse but isn't defined\n"); [],[],[]
      | Atom_Not_Used s -> print_string ("Atom \""^s^"\" is defined but never used\n"); [],[],[]
;;
