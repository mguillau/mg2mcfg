{
(** Returns a stream of lexemes defined in mtpglexer.mll
	@author Matthieu Guillaumin
	@see 'mtpglexer.mll' for lexemes used *)
open Mtpgparser;;
exception LexingError of int*string
}

rule token = parse
 [' ' '\t' '\n' '\r'] {token lexbuf}
| ( '/' [^'/']* '/' ) {token lexbuf} (* ignoring things between '/'s *)
| ( '%' [^'\n']* '\n' )   {token lexbuf} (* everything behind a % is ignored *)
| ')' {ENDRULE}
| '(' {BEGINRULE}
| '[' {BEGINORDER}
| ']' {ENDORDER}
| '<' {SMALLER}
| '>' {GREATER}
| '.' {DOT}
| ',' {COMA}
| ';' {SEMICOLON}
| '\'' {RIGHTINVERSE}
| '`' {LEFTINVERSE}
| (['a'-'z' 'A'-'Z' '0'-'9'])+ {IDENT(Lexing.lexeme lexbuf)}
| ('\"' ([^'\"' ';']* as idiom) '\"') {IDENT(idiom)}
| eof {ENDGRAMMAR}
| (_) {raise(LexingError(Lexing.lexeme_start lexbuf,Lexing.lexeme lexbuf))}
