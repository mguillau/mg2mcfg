%{
(** Parses the stream returned by the Mtpglexer lexer according to the grammar of mTPGs defined in mtpgparser.mly.
    Please note that your grammar should start with the definitions of its start symbols then the relations.
	@author Matthieu Guillaumin
	@see 'mtpgparser.mly' for the explicit accepted grammar *)
open Mtpgtransform.Type;;
open Mtpgtransform.Tuple;;
open Mtpgverify;;
%}

%token BEGINORDER ENDORDER BEGINRULE ENDRULE ENDGRAMMAR 
%token COMA SEMICOLON DOT SMALLER GREATER
%token LEFTINVERSE RIGHTINVERSE

%token <string> IDENT

%start mtpg
%type <string list * (string * string) list * Mtpgtransform.Tuple.t list> mtpg

%%

mtpg:
  startlist orderlist lexrulelist ENDGRAMMAR { check(true,$1,$2,$3) }
;
  
startlist:
  IDENT slist {$1::$2}
;

slist:
 {[]}
| IDENT slist {$1::$2}
;

orderlist:
 {[]}
| BEGINORDER IDENT SMALLER IDENT ENDORDER orderlist {($2,$4)::$6}
| BEGINORDER IDENT GREATER IDENT ENDORDER orderlist {($4,$2)::$6}
;

lexrulelist:
 {[]}
| BEGINRULE lexrule ENDRULE lexrulelist {$2::$4}
;

lexrule:
  couple {[$1]}
| couple SEMICOLON lexrule {$1::$3}
;

couple:
 typed COMA IDENT { {typ=$1; lex=$3} }
| typed COMA { {typ=$1; lex=""} }
;

typed:
 IDENT mode { [{mode=$2; name=$1}] }
| IDENT mode DOT typed { {mode=$2; name=$1}::$4}
;

mode:
  {ATOM}
| LEFTINVERSE {LEFT}
| RIGHTINVERSE {RIGHT}
;
%%
