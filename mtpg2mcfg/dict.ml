(** Implementation of "dictionaries" (association maps with integers). Translations between a given ordered type and integers are possible in both ways.
@author Matthieu Guillaumin *)

(** Input signature of the functor [Make] *)
module type OrderedType =
sig
  type t
  val compare: t -> t -> int
  val print: Pervasives.out_channel -> (int -> string) -> t -> unit
end

(** Output signature of the functor [Make] *)
module type S =
sig
  type key
  type t
  val empty: t
  val find: t -> key -> int
  val retrieve: t -> int -> key
  val print: Pervasives.out_channel -> (int -> string) -> (int -> key -> bool) -> t -> unit
  val exists: t -> key -> bool
end

(** Reduced [Map] signature *)
module type ShortMapType =
sig
  type key
  type 'a t
  val empty : 'a t
  val add : key -> 'a -> 'a t -> 'a t
  val find : key -> 'a t -> 'a
end

(** Functor building an implementation of a two-way dictionary associating an integer to an element of type the given ordered type *)
module Make(Ord: OrderedType) : (S with type key = Ord.t) =
struct

  (** One way of the dictionary ([key] -> [int]) is given with a [Map], the other with an [key] array *)
  module OMap : (ShortMapType with type key = Ord.t) = Map.Make(Ord)
  
  type key = Ord.t
  type data = D of key | Nil
  type t = { mutable counter: int;
             mutable data: int OMap.t;
	     mutable lookup: data array }
      
  let empty = { counter = (-1); data = OMap.empty; lookup = Array.make 1 Nil }

  (** Dynamically resize the array *)
  let resize map n =
    let olddata = map.lookup in
    let oldsize = Array.length olddata in
    let newsize = min (max n 1) Sys.max_array_length in
      if (newsize > oldsize) then 
	( let newdata = Array.init newsize (function i -> if i<oldsize then olddata.(i) else Nil)
	  in map.lookup <- newdata )

  (** Returns the corresponding integer or creates a new one if the argument is not in the map yet *)
  let find map k = try OMap.find k map.data
  with Not_found -> (map.counter <- succ map.counter;
		     map.data <- OMap.add k map.counter map.data;
		     if map.counter > Array.length map.lookup -1 then resize map (2*map.counter +1); 
		     map.lookup.(map.counter) <- D k;
		     map.counter)

  (** Returns [true] iif the element is already in the dictionary *)
  let exists map k = try ignore(OMap.find k map.data); true
  with Not_found -> false

  (** Returns the element corresponding to the given integer, or raises Not_found if this integer is not defined in the map *)
  let retrieve map i = if i>(-1) && i<map.counter+1 then (match map.lookup.(i) with D s -> s | _ -> raise Not_found) else raise Not_found

  (** Prints the dictionary, mapped with first function and filtered with second *)
  let print ch g f map =
    Array.iteri (fun k v' -> match v' with D v -> (if f k v && k<map.counter+1 then (output_string ch ("t"^string_of_int k^" : "); Ord.print ch g v; output_string ch "\n")) | Nil -> ())  map.lookup
end;;
