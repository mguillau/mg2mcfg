(** Defines the functions to treat the grammar depending on the wished input and output *)

open Mtpgtransform;;
open Mtpglexer;;
open Mtpgparser;;

module EndUser =
 struct

 exception No_Specified_Input
 exception Multiple_Input_Specified

 (** @return the grammar given in the given channel *)
 let load_gram_from_channel in_ch =
    let lexbuf = Lexing.from_channel in_ch in mtpg token lexbuf

 (** @return the grammar from standard input *)
 let load_gram_from_stdin() = load_gram_from_channel stdin

 (** @return the grammar specified in the file of given filename *)
 let load_gram_from_file s = load_gram_from_channel (open_in s)

 (** @return the grammar from the given string *)
 let load_gram_from_string s = let lexbuf = Lexing.from_string s in
     (mtpg token lexbuf)
       
 (** Outputs onto the given channel *)
 let process_gram_to_channel out_ch b (start,rel,g) dict =
   let ht = new journalizedTuples (List.length g,List.length rel) in
      ht#init_grammar g;
      ht#init_relations rel;
      prerr_string "converting"; flush stderr;
      ht#compute();
      ht#add_start_rules start;
      ht#clear();
      prerr_string "printing mcfg... "; flush stderr;
      ht#print_mcfg out_ch;
      flush out_ch;
      if b then close_out out_ch;
      prerr_string "ok\n";
      if (dict <> "") then (prerr_string "exporting dictionary... "; flush stderr;
			    let out_dict = open_out dict in
			    ht#dict out_dict;
			    prerr_string "ok\n")

 (** Outputs on standard output *)
 let process_gram_to_stdout  = process_gram_to_channel stdout true

 (** Outputs into the given file (overwrite mode) *)
 let process_gram_to_file s = process_gram_to_channel (open_out s) false

end;;

(** The main function. *)
let _ = 
 let usage_msg = "Usage: "^(Sys.argv.(0))^" [<filename>|-i] [-o <output>] [-dict <file>]"  in
 let output = ref "" and input = ref "" and input_stdin = ref false
 and dict = ref "" in 
 let specs = [("-i",Arg.Set input_stdin,  "     Option to set input to standard input instead of file <filename>");
              ("-o",Arg.Set_string output,"     Option to set output file to <output> (default: standard output)");
	      ("-dict", Arg.Set_string dict,   "  Option to export the dictionnary")]
 in let an_fun x = input:=x in
 try
  Arg.parse specs an_fun usage_msg;
  let st_rel_gr = 
  match !input, !input_stdin with
   | "",true -> prerr_string "/*** Press Ctrl-D when you are finished ***/\n";
		flush stderr;
		EndUser.load_gram_from_stdin()
   | "", false -> raise EndUser.No_Specified_Input
   | _, true -> raise EndUser.Multiple_Input_Specified
   | s, false -> EndUser.load_gram_from_file s
  in
  match !output with
   | ""-> prerr_string "\n/*** This is the corresponding MCFG ***/\n";
	   flush stderr;
           EndUser.process_gram_to_stdout st_rel_gr !dict
   | s -> EndUser.process_gram_to_file s st_rel_gr !dict
 with
  | EndUser.No_Specified_Input -> Arg.usage specs usage_msg
  | EndUser.Multiple_Input_Specified -> Arg.usage specs usage_msg
  | Mtpglexer.LexingError(i,s) -> prerr_string ("There is an error with your grammar at character "^(string_of_int i)^" with the string \""); prerr_string s; prerr_string "\".\n"
  | Parsing.Parse_error -> prerr_string "Parsing Error!\nYour grammar is not well formed: please refer to the documentation\n"
  | Failure s -> prerr_string "An error has occured: \""; prerr_string s; prerr_string "\"\nYou should check your grammar for mispelling\n and make sure you use only alphabetic characters for vocabulary and features.\n"
 ;;
