(** The module for transforming a mTPG to a MCFG
@author Matthieu Guillaumin *)



module AssocStrings = Dict.Make(
struct
 type t = string
 let compare = Pervasives.compare
 let print out_ch g = output_string out_ch 
end);;

module IntSet = Set.Make(
struct
 type t = int
 let equal x y = x=y
 let hash x = x
 let compare x y = x-y
end);;



(** Defining types within mTPG *)
module Type =
struct
 (** An element of a type can be an atom, a left inverse or a right inverse *)
 type mode = ATOM | LEFT | RIGHT

 (** Named type elements *)
 type t = {mode: mode; name: string}
end;;


(** Defining mcfg rules *)
module Mcfg =
struct
 type tuple = int
 type idiom = int
 type length = int
 type index = int
 type mode = LEFT | RIGHT
 type perm = int list		(* additionnal permutation of types, to have an ordered tuple *)  
 type left = Start
             | NonTerminal of tuple
             | Lexical of tuple * int * int (* a tuple, the value number, the node number *)
	     | Idiom of idiom * int (* a idiom, the node number *)
 type right = Gcon of tuple * length * index * index * mode
              | Conc of tuple * length * tuple * length * perm
              | Simple of tuple
              | LexRoot of tuple * int * int * length * perm
              | LexNode of tuple * int * int * length * int * length
	      | IdiomRoot of idiom * int
	      | IdiomNode of idiom * int * int
              | LexTerminal of string
 type t = left * right
     
 let compare = Pervasives.compare

 (** Prints a mcfg rule, generating the map function in the process *)
 let print_left out_ch l = 
   let pr = output_string out_ch
   and pri i = output_string out_ch (string_of_int i) in
   let prt() = pr "t" and pru() = pr "_" in
     (match l with
	| Start -> pr "S"
	| NonTerminal i -> prt(); pri i
	| Lexical (i,j,k) -> prt(); pri i; pru(); pri j; pru(); pri k
	| Idiom (i,j) -> pr "i"; pri i; pru(); pri j;)

 let print_right out_ch r = 
   let pr = output_string out_ch
   and pri i = output_string out_ch (string_of_int i) in
   let prt() = pr "t" and prs() = pr " " and pru() = pr "_" in
   let rec rpr a d n = if d<n+1 then ([a,d]::(rpr a (d+1) n)) else [] in
   let rec rpr2 i j m n = function 
     | d when d>n -> []
     | d when d=j -> rpr2 i j m n (d+1)
     | d when d=i -> (match m with RIGHT -> [0,j;0,i] | LEFT -> [0,i;0,j])::(rpr2 i j m n (d+1))
     | d -> [0,d]::(rpr2 i j m n (d+1)) in
   let prl = List.iter (function l -> pr "[";
			  match l with
			    | [] -> failwith "empty map function!"
			    | (c,d)::q -> (pri c; pr ","; pri d; List.iter (function (a,b) -> pr ";"; pri a; pr ","; pri b) q; pr "]")) in
   let inverse_perm l = 
     let arr = Array.make (List.length l) 0 in
     let rec inverse n = function
       | [] -> ()
       | t::q -> arr.(t) <- n; inverse (n+1) q
     in inverse 0 l; Array.to_list arr
   in
   let sort_combine p_ref l = let (_,r) = List.split (List.fast_sort (fun (a,_) (c,_) -> a-c) (List.combine (inverse_perm p_ref) l)) in r
   in (match r with
	| LexTerminal s -> pr "\""; pr s; pr "\""
	| IdiomRoot (i,n) -> pr "i"; pri i; pru(); pri n; prs(); pr " [0,0]"
	| IdiomNode (i,n1,n2) ->  pr "i"; pri i; pru(); pri n1; prs(); pr "i"; pri i; pru(); pri n2; prs(); pr " [0,0;1,0]"
	| LexNode (i,j,k1,l1,k2,l2) -> prt(); pri i; pru(); pri j; pru(); pri k1; prs(); prt();
	    pri i; pru(); pri j; pru(); pri k2; prs(); prl (rpr 0 0 l1 @ rpr 1 0 l2)
	| LexRoot (i,j,k,n,p) -> prt(); pri i; pru(); pri j; pru(); pri k; prs(); let p1 = rpr 0 0 n in prl (sort_combine p p1)
	| Simple t -> prt(); pri t; pr " [0,0]"
	| Conc (t1,l1,t2,l2,p) -> prt(); pri t1; prs(); prt(); pri t2; prs(); let p1 = (rpr 0 0 l1)@(rpr 1 0 l2) in prl (sort_combine p p1)
	| Gcon (t,n,i,j,m) -> prt(); pri t; prs(); prl( rpr2 i j m n 0 ));
     pr "\n"


 let print out_ch (l,r) = print_left out_ch l; output_string out_ch " --> "; print_right out_ch r

 let following (_,r) = match r with
   | LexTerminal _ -> []
   | IdiomNode (i,n1,n2) -> [ Idiom (i,n1); Idiom (i,n2) ]
   | IdiomRoot (i,n) -> [ Idiom (i,n) ]
   | LexNode (i,j,k1,_,k2,_) -> [ Lexical(i,j,k1); Lexical(i,j,k2) ]
   | LexRoot (i,j,k,_,_) -> [ Lexical(i,j,k) ]
   | Simple t -> [ NonTerminal t ]
   | Conc (t1,_,t2,_,_) -> [ NonTerminal t1; NonTerminal t2 ]
   | Gcon (t,_,_,_,_) -> [ NonTerminal t ]

end;;





module AssocLeftMcfg = Dict.Make(
struct
  type t = Mcfg.left
  let compare = Pervasives.compare
  let print out_ch g = Mcfg.print_left out_ch
end);;
module AssocRightMcfg = Dict.Make(
struct
  type t = Mcfg.right
  let compare = Pervasives.compare
  let print out_ch g = Mcfg.print_right out_ch
end);;





(** Defining types and functions for tuples *)
module Tuple =
 struct
 (** The basic typed string as gathered for the input grammar *)
 type basic_couple = {typ: Type.t list; lex: string}

 (** Tuples of basic typed strings *)
 type basic_t = basic_couple list

 (** Typed strings after checking by the Mtpgverify module *)
 type typed_string = {left: string list; atom: string; right: string list; value: string}

 (** Tuples as we are going to manipulate them *)
 type t = typed_string list

 let is_idiom s =
   try
     let i = String.index s ' ' in i>=0
   with Not_found -> false
     
 let split_idiom s =
   let rec aux buf index words str = match index,str.[index] with
     | 0,' ' -> (if buf <> "" then [buf] else [])@words
     | 0,c -> (Char.escaped c ^ buf)::words 
     | _,' ' -> aux "" (index -1) ((if buf<>"" then [buf] else [])@words) str
     | _,c -> aux (Char.escaped c ^ buf) (index -1) words str
   in aux "" (String.length s -1) [] s
	
 (** @return the list of MCFG rules to generate the given idiom, numbered as the given integer *)
 let gen_idiom_mcfg x y z n s =
   let l = split_idiom s in
   let itemize m s = (Mcfg.Idiom (n,m),Mcfg.LexTerminal s) in
   let rec split = function
     | [] -> [],[]
     | [t] -> [t],[]
     | t1::t2::q -> let l1,l2 = split q in (t1::l1),(t2::l2) in
   let rec map f1 f2 k l = function
     | [],[] -> l,[],k 
     | [t],[] -> l,[t],k
     | (t::q),(t'::q') -> let (a,b,c) = map f1 f2 (k+1) l (q,q') in
         ((f1 k t t')::a),((f2 k t t')::b),c
     | _ -> raise(Invalid_argument "The list lengths don't match") in
   let f1 k s1 s2 = (Mcfg.Idiom (n,k), Mcfg.IdiomNode (n,s1,s2)) in
   let f2 k _ _ = k in
   let rec aux_step k l = function
     | [] -> l
     | [_] -> l
     | next -> let (a,b,c) = map f1 f2 k l (split next) in aux_step c a b   in
   let last m = (Mcfg.Lexical (x,y,z),Mcfg.IdiomRoot (n,m)) in
   let rec aux_init = function
     | 0 -> []
     | n -> n::(aux_init (n-1))
   in
   let w = List.length l in 
   let init = List.rev (aux_init w) in
   let items = List.map2 itemize init l in
   let steps = aux_step (w+1) [] init in
     (last (2*w-1))::(items@steps)
       (* the created balanced binary tree has always 2n-1 internal node, ie steps *)
       
 (** Generates the mcfg rules for building the tuple which number is also given. The result is a balanced binary tree. *)
 let gen_mcfg j r n p perm =
   let itemize m s = if is_idiom s then (j := !j+1; gen_idiom_mcfg n p m !j s)
   else [Mcfg.Lexical (n,p,m),Mcfg.LexTerminal s] in
   let itemize2 m = (0,m) in (* first element (integer) is arity *)
   let rec split = function
     | [] -> [],[]
     | [t] -> [t],[]
     | t1::t2::q -> let l1,l2 = split q in (t1::l1),(t2::l2) in
   let rec map f1 f2 k l = function
     | [],[] -> l,[],k
     | [t],[] -> l,[t],k
     | (t::q),(t'::q') -> let (a,b,c) = map f1 f2 (k+1) l (q,q') in
         ((f1 k t t'::a),(f2 k t t'::b),c)
     | _ -> raise(Invalid_argument "The list lenghts do not match") in
   let f1 k (a,m1) (b,m2) = (Mcfg.Lexical (n,p,k), Mcfg.LexNode (n,p,m1,a,m2,b)) in
   let f2 k (a,_) (b,_) = (a+b+1),k in
   let rec aux_step k l = function
     | [] -> l
     | [_] -> l;
     | next -> let (a,b,c) = map f1 f2 k l (split next) in aux_step c a b in
   let last q = (Mcfg.NonTerminal n, Mcfg.LexRoot (n,p,q,List.length r -1,perm)) in
   let rec aux_init f = function
     | n when n>f -> []
     | n -> n::(aux_init f (n+1)) in
   let z = List.length r in
   let init = (aux_init (z-1) 0) in
   let items = List.map2 itemize init (List.map (function x-> x.value) r) in
   let items2 = List.map itemize2 init in
   let steps = aux_step z [] items2 in
     (last (2*(z-1)))::(List.concat (steps::items))
       
end;;








(** The types and functions for closing rules with [conc] are [gcon] *)
module ATuple =
struct
  exception No_Atom_Unicity
    
  (** Anonymous typed string *)
  type typed_string = {left: int list; atom: int; right: int list}
      
  (** Anonymous tuple *)
  type t = typed_string list

  let compare = Pervasives.compare

  let print out_ch g tuple =
    let pr = output_string out_ch in
    let f i = output_string out_ch (g i) in
    let f' i = f i; pr "'" in
    let f'' i = f i; pr "`" in
    let g t l = match l with [] -> () | h::q -> t h; List.iter (fun x -> pr ";"; t x) q in
    let g2 l = List.iter (fun x -> f' x; pr ".") l in
    let g3 l = List.iter (fun x -> pr "."; f'' x) l in
    let g4 k = g2 k.left; f k.atom; g3 k.right in 
    let g' = g g4 in
      pr "(";
      g' tuple;
      pr ")"
      
  let equal a b = a=b
      
  (** Determines if the typed string is saturated *)
  let is_saturated ts = ts.left = [] && ts.right = [] 
						     
(*  let print out_ch a = (String.concat ";" (List.map (function c -> String.concat "<" (List.map string_of_int c.left) ^ "<" ^ string_of_int c.atom ^ ">"  ^ String.concat ">" (List.map string_of_int c.right)) a)) *)

  let convert_typed_string tbl r = {left = List.map (AssocStrings.find tbl) r.Tuple.left;
				    right = List.map (AssocStrings.find tbl) r.Tuple.right;
				    atom = AssocStrings.find tbl r.Tuple.atom}
  let from_tuple tbl = List.map (convert_typed_string tbl)
		     
 (** Returns a saturated typed string with the given string as atom *)
  let make_saturated tbl x = {left = []; right = []; atom = AssocStrings.find tbl x}

  (** Sorts a tuple *)
  let sort l =
    let rec init n = function
      | [] -> []
      | t::q -> (t,n)::(init (n+1) q)
    in let rec split = function
      | [] -> [],[]
      | [t] -> [t],[]
      | t1::t2::q -> let a,b = split q in (t1::a,t2::b)
    in let rec merge = function
      | [],l -> l
      | l,[] -> l
      | ((id1,pos1)::q1),((id2,pos2)::q2) when id1.atom<id2.atom -> (id1,pos1)::(merge(q1,((id2,pos2)::q2)))
      | l,(h2::q2) -> h2::(merge (l,q2))
    in let rec aux_sort = function
      | [] -> []
      | [t] -> [t]
      | l -> let a,b = split l in merge (aux_sort a,aux_sort b)
    in List.split (aux_sort (init 0 l))

  let concsort l1 l2 n1 =
    let rec init n = function
      | [] -> []
      | t::q -> (t,n)::(init (n+1) q)
    in let rec merge = function
      | [],l -> l
      | l,[] -> l
      | ((id1,pos1)::q1),((id2,pos2)::q2) when id1.atom<id2.atom -> (id1,pos1)::(merge(q1,((id2,pos2)::q2)))
      | l,(h2::q2) -> h2::(merge (l,q2))
    in List.split (merge (init 0 l1,init n1 l2))

 let unicize l =
  let rec aux = function
   | [] -> []
   | [t] -> [t]
   | t::t'::q when t=t' -> aux (t'::q)
   | t::q -> t::(aux q)
  in aux (List.fast_sort compare l)

  (** Checks if the given tuple complies with the atom unicity constraint
    @raise No_Atom_Unicity if not*)
  let rec atom_unicity = function
      | [] -> ()
      | [_] -> ()
      | t1::t2::_ when t1.atom=t2.atom -> raise No_Atom_Unicity
      | _::q -> atom_unicity q
	 
  (** Determines if a tuple complies with the atom unicity constraint *)
 let complies_atom_unicity t = try atom_unicity t; true with No_Atom_Unicity -> false

 (** Checks that every duplicate category in a temporarly unproper tuple have an inverse in the tuple. This is a necessary but not suffisent condition for having a proper tuple after gcon closure *)
 let has_a_chance lo (t1,_,_) (t2,_,_) =
   let rec intersection g1 g2 = match g1,g2 with
   | [],_ -> []
   | _,[] -> []
   | (h1::q1),(h2::q2) when h1.atom = h2.atom -> h1.atom::(intersection q1 q2)
   | (h1::q1),(h2::q2) when h1.atom < h2.atom -> intersection q1 (h2::q2)
   | (h1::q1),(_::q2) -> intersection (h1::q1) q2
   in let dom_inter = intersection t1 t2 in
   let smaller l = List.concat (List.map (function n -> (n::(lo n))) l) in
   let inv' = unicize (List.fold_right 
			 (fun ts l -> ts.left@ts.right@l) t1
			 (List.fold_right (fun ts l -> ts.left@ts.right@l) t2 [])) in
   let inv = unicize (smaller inv') in
   let rec is_included g1 g2 = match g1,g2 with
     | [],_ -> true
     | _,[] -> false 
     | (h1::q1,h2::q2) when h1=h2 -> is_included q1 q2
     | (h1::_,h2::_) when h1<h2 -> false
     | (h1::q1,_::q2) -> is_included (h1::q1) q2
   in is_included dom_inter inv
   
 (** Returns the concatenation via the [conc] rule of the tuples t1 and t2 *)
 let conc tr (t1,n1,l1) (t2,n2,l2) =
   let t3',perm = concsort t1 t2 (List.length t1) in
   let n3 = tr t3' and l3 = l1+l2 in
   (t3',n3,l3),(Mcfg.NonTerminal n3,Mcfg.Conc (n1,l1-1,n2,l2-1,perm))
     
 (** Removes the [k]th element from a list *)
 let rec remove k t = match (k,t) with
   | _,[] -> failwith("error1")
   | 0,(_::q) -> q
   | n,(x::q) -> x::(remove (n-1) q)
       
 (** Given a type of inverse and a typed string, removes the edge inverse of that type from the type *)
 let red ts = function
   | Type.LEFT -> {left = ts.left; atom = ts.atom; right = List.tl ts.right}
   | _ -> {left = List.tl ts.left; atom = ts.atom; right = ts.right}
	
 (** Applies [red] to the [i]th element of a list *)
 let rec cut i side t = match (i,t) with
     | _,[] -> failwith "error"
     | 0,(x::q) -> (red x side)::q
     | _,(x::q) -> x::(cut (i-1) side q)
	 
 (** Applies [gcon] to elements [i] and [j] (which is a inverse of the given type) in tuple t *)
 let gcon tr i j side (t,n,l) = 
   let t' = cut i side t in
   let t'' = remove j t' in
   let n'' = tr t'' in
   (t'',n'',l-1), (Mcfg.NonTerminal n'',
	 Mcfg.Gcon (n,l-1,i,j,match side with Type.RIGHT -> Mcfg.RIGHT | Type.LEFT -> Mcfg.LEFT | _ -> failwith "IMPOSSIBLE is possible!!"))
       
 (** Returns the list of all elements associated to the given object in the list *)
 let rec assoc_all s = function
   | [] -> []
   | (a,b)::q when s=a -> b::(assoc_all s q)
   | _::q -> assoc_all s q
       
 (** Given a tuple, returns the list of saturated typed strings (and their positions) and the list of inverses (with their position and type), applying relations *)
 let rec analyze lower n sat unsat =
     function
       | [] -> sat,unsat
       | ts::tl -> let sat',unsat' = analyze lower (n+1) sat unsat tl in
           if is_saturated ts then ((ts.atom,n)::sat',unsat')
           else (sat', (match ts.left with
                          | [] -> []
                          | x::_ -> List.map (function e -> (e,(n,Type.RIGHT))) (x::(lower x)) )
                   @(match ts.right with
                       | [] -> []
                       | x::_ -> List.map (function e -> (e,(n,Type.LEFT))) (x::(lower x))  )
                   @unsat')
	     

 (** Recursive function to calculate all the possible rules and final tuples for the closure by gcon of the given tuple *)
 let rec gcon_closure tr lower (t,n,l) =
   let (sat,unsat) = analyze lower 0 [] [] t
   in let sat_dest = List.map (function (x,_) -> assoc_all x unsat) sat
   in let possible_gcons = List.flatten (List.map2 (fun (_,r) s -> List.map (function (p,q) -> (p,r,q) ) s) sat sat_dest)
   in let applied_gcons = List.map (function (x,y,z) -> gcon tr x y z (t,n,l)) possible_gcons
   in
     match applied_gcons with
       | [] -> (if complies_atom_unicity t then [(t,n,l)] else []),[]
       | l  -> (let tuple_list,mcfg_list = List.split l
		in let tuples,rules = List.split (List.map (gcon_closure tr lower) tuple_list)
		in let full_list = List.combine tuples (List.map2 (fun a li -> a::li) mcfg_list rules)
		in let good_list = List.filter (fun (x,_) -> x<>[]) full_list
		in let new_t,new_r = List.split good_list 
		in unicize(List.flatten new_t), List.flatten new_r)

end;;






(** To associate an unique integer for each tuple *)
module AssocTuple = Dict.Make(ATuple);;





(** The main class, containing sets and hash-tables to compute all the mcfg rules *)
class journalizedTuples(n,nr)=
object(self)

  val keys = AssocStrings.empty
  val tuple = AssocTuple.empty
  
  method get_key = AssocStrings.find keys
  method get_cat = AssocStrings.retrieve keys
  method tr = AssocTuple.find tuple
  method get_tuple = AssocTuple.retrieve tuple

  (** The hash-table of relations *)
  val greater = Dyn_array.create nr
  val lower = Dyn_array.create nr	     
 
  method init_relations relat = let a,b = List.split relat in
    List.iter2 (fun x y -> Dyn_array.add greater x y; Dyn_array.add lower y x)
      (List.map self#get_key a) (List.map self#get_key b)
					      
  (** Returns all the instantiations of the category, i.e every 'smaller' items in relation to the given category *)
  method lower = Dyn_array.find lower
  method greater = Dyn_array.find greater

	
  val leftdict = AssocLeftMcfg.empty
  val rightdict = AssocRightMcfg.empty
  val mcfg = Dyn_array.create n
  val printed_mcfg = Dyn_array.create n

  method tr_left = AssocLeftMcfg.find leftdict
  method tr_right = AssocRightMcfg.find	rightdict
  method rt_left = AssocLeftMcfg.retrieve leftdict
  method rt_right = AssocRightMcfg.retrieve rightdict
 
  (** Add an element to the collection of mcfg *)
  method addmcfg (l,r) =
    let l' = self#tr_left l and r' = self#tr_right r
    in Dyn_array.add mcfg l' r'

  (** Notify that a mcfg rule has been printed *)
  method addprintedmcfg (l,r) =
    let l' = self#tr_left l and r' = self#tr_right r
    in Dyn_array.add printed_mcfg l' r'

  (** Returns the list of mcfg rules having its left part inside the given list *)
  method find_mcfg_beginning_inside leftlist =
    let rightlistlist = List.map (function l -> Dyn_array.find mcfg (self#tr_left l)) leftlist
    in let shortlist = List.combine leftlist rightlistlist
    in let rules = List.map (function (l,rl) -> List.map (function r -> (l,self#rt_right r)) rl) shortlist
    in List.concat rules

  (** The set containing old saturated tuples *)
  val mutable oldsaturated = IntSet.empty

  (** The set containing new saturated tuples *)
  val mutable newsaturated = IntSet.empty

  (** The set containing current saturated tuples *)
  val mutable temporaryset = IntSet.empty

  (** The hash table containing old unsaturated tuples *)
  val oldunsaturated = Dyn_array.create n
			 
  (** The hash table containing new unsaturated tuples *)
  val mutable newunsaturated = Dyn_array.create n
			 
  (** The hash table containing current unsaturated tuples *)
  val temporaryhashtbl = Dyn_array.create n
			   
  (** A marker to tell if the content has changed *)
  val mutable has_changed = false

  (** Prints the correspondance between mcfg symbols and tuples *)
  method dict out_ch =
    let pr = output_string out_ch in
      AssocTuple.print out_ch self#get_cat (fun v _ -> List.length (Dyn_array.find printed_mcfg (self#tr_left (Mcfg.NonTerminal v))) > 0) tuple;
      pr "\n"

  (** Find all the old tuples which select the given category *)
  method old_elements x =
    let rec unicize = function
	[] -> []
      | [t] -> [t]
      | t1::t2::q when t1=t2 -> unicize (t2::q)
      | t1::q -> t1::(unicize q) 
    in unicize (List.fast_sort Pervasives.compare (List.flatten (List.map (Dyn_array.find oldunsaturated) (x::self#greater x))))
			  
  (** Find all the new tuples which select the given category *)
  method new_elements x =
    let rec unicize = function
	[] -> []
      | [t] -> [t]
      | t1::t2::q when t1=t2 -> unicize (t2::q)
      | t1::q -> t1::(unicize q) 
    in unicize(List.fast_sort Pervasives.compare (List.flatten (List.map (Dyn_array.find newunsaturated) (x::self#greater x))))
			  
  (** Add a tuple to the collection. If it is saturated, then its ID is added to the current tuple set, if not, its ID is added in the hash-table. *)
  method addtuple (x,id,_) =
    let aux2 = function  
      | [] -> ()
      | t::_ -> if not(List.mem id (self#old_elements t) || List.mem id (self#new_elements t))
        then (Dyn_array.add temporaryhashtbl t id;
	      has_changed <- true);  
    in
    let aux k = 
      aux2 k.ATuple.left;
      aux2 k.ATuple.right
    in
      List.iter aux x;
      if (List.for_all ATuple.is_saturated x)
      then ( if not(IntSet.mem id oldsaturated || IntSet.mem id newsaturated) then
               ( temporaryset <- IntSet.add id temporaryset;
		 has_changed <- true )
           )
	
  (** Returns all the old saturated tuples *)
  method old_saturated = IntSet.elements oldsaturated
			   
  (** Returns all the new saturated tuples *)
  method new_saturated = IntSet.elements newsaturated

  method clear() = 
    oldsaturated <- IntSet.empty;
    newsaturated <- IntSet.empty;
    temporaryset <- IntSet.empty;
    Dyn_array.clear oldunsaturated;
    Dyn_array.clear newunsaturated;
    Dyn_array.clear temporaryhashtbl

  (** Declares the end of a step in the calculation: the new elements are added to the old ones, and current ones replace the new, and the current sets and hash tables are emptied *)
  method next_step() = 
    oldsaturated <- IntSet.union oldsaturated newsaturated;
    newsaturated <- temporaryset;
    temporaryset <- IntSet.empty;
    Dyn_array.iter (fun k v -> Dyn_array.add oldunsaturated k v) newunsaturated;
    newunsaturated <- Dyn_array.copy temporaryhashtbl;
    Dyn_array.clear temporaryhashtbl;
    has_changed <- false
      
  (** Takes a grammar, i.e a list of tuples, and adds them to the collection of tuples, as well as all the mcfg rules to build them *)
  method init_grammar g = let j = ref 0 in
    ignore(List.fold_right (fun t p -> let at,perm = ATuple.sort(ATuple.from_tuple keys t) in
			    let nt = self#tr at in
			      List.iter self#addmcfg (Tuple.gen_mcfg j t nt p perm);
			      self#addtuple (at,nt,List.length t); p+1) g 0)
      
  (** Prints out on the given channel the set of accessible mcfg rules *)
  method print_mcfg out_ch =
    let rec print_aux m =
      let (l,r) = m in let l' = self#tr_left l and r' = self#tr_right r in
      if not(List.mem r' (Dyn_array.find printed_mcfg l')) then
	( Mcfg.print out_ch m;
	  self#addprintedmcfg m;
	  let follow = Mcfg.following m
	  in if follow <> [] then List.iter print_aux (self#find_mcfg_beginning_inside follow))
    in List.iter print_aux (self#find_mcfg_beginning_inside [Mcfg.Start])
	 
  (** Main function: closes the collection of tuples with [conc] and [gcon] using [gcon_closure] *)
  method compute() =
    let f2 tuple1 tuple2 =
      let (_,id1,_) = tuple1 and (_,id2,_) = tuple2 in
      if ATuple.has_a_chance self#lower tuple1 tuple2 then (
      let t1,str1 = ATuple.conc self#tr tuple2 tuple1 in (* applies conc *)
      let tl2,strl2 = ATuple.gcon_closure self#tr self#lower t1 in
	(* closes the tuple by gcon: list of proper tuples (possible ambiguity)
	   and list of mcfg rules *)
	if tl2 <> [] then (
	  List.iter self#addtuple tl2;
	  List.iter self#addmcfg (str1::strl2)))
    in
    let f1 func id =
      let t = self#get_tuple id
      in let len = List.length t
      in let rec aux = function
	| ([]) -> ()
	| (ts::q) -> (let l = func ts.ATuple.atom in let l' = List.map (function d -> let t' = self#get_tuple d in (t',d,List.length t') ) l
		      in List.iter (f2 (t,id,len)) l'; aux q)
      in aux t
    in
      
    let init_time = Unix.gettimeofday() in
    let round x = float ( truncate(100. *. x)) /. 100. in
      while has_changed do
	self#next_step();
	prerr_string ".";
        flush stderr;
	IntSet.iter (f1 self#new_elements) newsaturated;
	IntSet.iter (f1 self#old_elements) newsaturated;
	IntSet.iter (f1 self#new_elements) oldsaturated;
      done;
      prerr_string " ok ("; prerr_float (round (Unix.gettimeofday() -. init_time)); prerr_string " seconds)\n"
	
  (** Add to mcfg set the starting rules *)
  method add_start_rules (s:string list) =
    let make_start_mcfg w = (Mcfg.Start, Mcfg.Simple (self#tr [ATuple.make_saturated keys w])) in
      List.iter self#addmcfg (List.map make_start_mcfg s)
	
  (** Include every rule corresponding to relations, useful in the 'greater' part is a start category -- disabled for now, the only limitation being that you can't have starting symbols involved int relations other than equality *)
  method add_relations relat = 
    let a,b = List.split relat in
      List.iter2 (fun x y -> let x' = [ATuple.make_saturated keys x] and y'= [ATuple.make_saturated keys y] in
		  let nx = self#tr x' and ny = self#tr y' in
		    if IntSet.mem ny oldsaturated then self#addmcfg (Mcfg.NonTerminal nx, Mcfg.Simple ny))
	b a

end;;
