(** Defines the functions to treat the grammar depending on the wished input and output *)

open Hmgtransform;;
open Hmglexer;;
open Hmglexer_pl;;
open Hmgparser;;
open Hmgparser_pl;;

module EndUser =
 struct

 exception No_Specified_Input
 exception Multiple_Input_Specified

 (** @return the grammar given in the given channel *)
 let load_gram_from_channel b in_ch =
    let lexbuf = Lexing.from_channel in_ch in
    match b with false -> (mg token lexbuf) | true -> (mg_pl token_pl lexbuf)

 (** @return the grammar from standard input *)
 let load_gram_from_stdin b = load_gram_from_channel b stdin

 (** @return the grammar specified in the file of given filename *)
 let load_gram_from_file b s = load_gram_from_channel b (open_in s)

 (** @return the grammar from the given string *)
 let load_gram_from_string b s = let lexbuf = Lexing.from_string s in match b with
     false -> (mg token lexbuf) | true -> (mg_pl token_pl lexbuf)

 (** Outputs onto the given channel *)
 let process_gram_to_channel out_ch (s,g,adj) dict =
    let ht = new aRuleChart (s,List.length g,adj) in
    ht#acquire_grammar g;
    ht#compute();
    ht#print_mcfg out_ch;
    if dict<>"" then (ht#dict (open_out dict))

 (** Outputs on standard output *)
 let process_gram_to_stdout = process_gram_to_channel stdout

 (** Outputs into the given file (overwrite mode) *)
 let process_gram_to_file s = process_gram_to_channel (open_out s)

end;;

(** The main function. *)
let _ = 
 let usage_msg = "Usage: "^(Sys.argv.(0))^" [-pl] [<filename>|-i] [-o <output>] [-dict <file>]"  in
 let output = ref "" and input = ref "" and input_stdin = ref false and input_pl = ref false and output_mg = ref "" and dict = ref "" in 
 let specs = [("-pl", Arg.Set input_pl,       "    Option to set input format to prolog format");
              ("-i",Arg.Set input_stdin,     "     Option to set input to standard input instead of file <filename>");
              ("-o",Arg.Set_string output,   "     Option to set output file to <output> (default: standard output)");
	      ("-dict",Arg.Set_string dict,  "  Output translation of mcfg symbols to file <file>")]
 in let an_fun x = input:=x in
 try
  Arg.parse specs an_fun usage_msg;
  let s_g_adj = 
  match !input, !input_stdin, !output_mg with
   | "",true,_ -> prerr_string "/*** Press Ctrl-D when you are finished ***/\n";
		flush stderr;
		EndUser.load_gram_from_stdin !input_pl
   | "", false,_ -> raise EndUser.No_Specified_Input
   | _, true,_ -> raise EndUser.Multiple_Input_Specified
   | s, false,_ -> EndUser.load_gram_from_file !input_pl s
  in
  match !output with
   | "" -> prerr_string "\n/*** This is the corresponding MCFG ***/\n";
	   flush stderr;
           EndUser.process_gram_to_stdout s_g_adj !dict
   | s -> EndUser.process_gram_to_file s s_g_adj !dict
 with
  | EndUser.No_Specified_Input -> Arg.usage specs usage_msg
  | EndUser.Multiple_Input_Specified -> Arg.usage specs usage_msg
  | Hmglexer.LexingError(i,s) -> prerr_string ("There is an error with your grammar at character "^(string_of_int i)^" with the string \""); prerr_string s; prerr_string "\".\n"
  | Parsing.Parse_error -> prerr_string "Parsing Error!\nYour grammar is not well formed: please refer to the documentation\n"
  | Hmglexer_pl.LexingError(i,s) -> prerr_string ("There is an error with your grammar at character "^(string_of_int i)^" with the string \""); prerr_string s; prerr_string "\".\n"
  | Failure s -> prerr_string "An error has occured: \""; prerr_string s; prerr_string "\"\nYou should check your grammar for mispelling\n and make sure you use only alphabetic characters for vocabulary and features.\n"
 ;;
