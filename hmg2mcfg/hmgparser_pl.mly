%{
(** Parses the stream returned by the Hmglexer_pl lexer according to the grammar of hMGs defined in hmgparser_pl.mly.
    Please note that your grammar should start with a line defining its start symbols.
	@author Matthieu Guillaumin
	@see 'hmgparser_pl.mly' for the explicit accepted grammar *)
open Hmgtransform.Feature;;
open Hmgtransform.Rule;;
open Hmgtransform.Adjunction;;
open Hmgtransform.LexItem;;
open Hmgverify;;

%}

%token ENDRULE ENDGRAMMAR
%token DEFINE
%token EQUAL PLUS MINUS GREATER SMALLER
%token LEFT RIGHT COMA START RIGHTPAR

%token <string> IDENT

%start mg_pl
%type <string list * Hmgtransform.Rule.grammar * Hmgtransform.Adjunction.t list> mg_pl

%%

mg_pl:
  lexrulelist startlist ENDGRAMMAR {check($2,$1)}
;

startlist:
 start {[$1]}
| start startlist {$1::$2}
;

start:
  START IDENT RIGHTPAR ENDRULE {$2}
;

lexrulelist:
 {[]}
| LEFT lexrule RIGHT ENDRULE lexrulelist {$2::$5}
;

lexrule:
 catlicenseelist RIGHT sign LEFT featurelist
     { let a,_ = $1 in (Rule {voc=a; nature=$3; features=($5);}) }
| catlicenseelist RIGHT side LEFT adjunc
     { let _,b = $1 and c=$5 in
       (Adjunction {side=$3;
                    leftcat=List.hd(b); left=List.tl(b);
                    rightcat=List.hd(c); right=List.tl(c);}) }
;

sign:
  DEFINE {LEXICAL}
;

side:
  GREATER GREATER {LEFT}
| SMALLER SMALLER {RIGHT}
;

adjunc:
 IDENT licenseelist { {name=$1; modifier=CATEGORY}::$2 }

catlicenseelist:
  {"",[]}
| IDENT {$1,[{name=$1; modifier=CATEGORY}]}
| licensee {$1.name,[$1]}
| IDENT COMA catlicenseelist {let a,b=$3 in ($1^" "^a),({name=$1; modifier=CATEGORY}::b)}
| licensee COMA catlicenseelist {let a,b=$3 in ($1.name^" "^a),($1::b)}
;

featurelist:
  feat {[$1]}
| feat COMA featurelist {$1::$3}
;

feat:
  prefeatmod IDENT { {name=$2; modifier=$1} }
| IDENT postfeatmod { {name=$1; modifier=$2} }
;

prefeatmod:
  MINUS {LICENSEE}
| EQUAL {SELECTOR}
| PLUS  {LICENSOR}
| SMALLER EQUAL EQUAL {AFFIXHOP_L}
| EQUAL GREATER {HEADMV_L}
;

postfeatmod:
| {CATEGORY}
| EQUAL EQUAL GREATER {AFFIXHOP_R}
| SMALLER EQUAL {HEADMV_R}
;

licenseelist:
  {[]}
| COMA licensee licenseelist {$2::$3}
;

licensee:
  MINUS IDENT { {name=$2; modifier=LICENSEE} }
;
%%
