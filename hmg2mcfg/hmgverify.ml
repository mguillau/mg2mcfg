(** Defines the functions to make simple verifications on the grammar
    @author Matthieu Guillaumin
 *)

open Hmgtransform.Feature;;
open Hmgtransform.Rule;;
open Hmgtransform.Adjunction;;
open Hmgtransform.LexItem;;

(** Creates 7 lists of feature names splitting them according to their type and remembering the rule number in which they occur, the different head movements and affix hoppings are treated likewise here *)
let rec build_lists m rules =
 let rec add n x y = match x with
   | [] -> y
   | t::q when is_cat t ->
                  let (a,b,c,d,e,f,g) = add n q y in ((t.name,n)::a,b,c,d,e,f,g)
   | t::q when is_sel t ->
                  let (a,b,c,d,e,f,g) = add n q y in (a,(t.name,n)::b,c,d,e,f,g)
   | t::q when is_lic t ->
                  let (a,b,c,d,e,f,g) = add n q y in (a,b,(t.name,n)::c,d,e,f,g)
   | t::q when is_lee t ->
                  let (a,b,c,d,e,f,g) = add n q y in (a,b,c,(t.name,n)::d,e,f,g)
   | t::q ->      let (a,b,c,d,e,f,g) = add n q y in (a,(t.name,n)::b,c,d,(t.name,n)::e,f,g)
 in match rules with 
 [] -> [],[],[],[],[],[],[]
 | (Rule t)::q -> add m t.features (build_lists (m+1) q)
 | (Adjunction t)::q -> let a,b,c,d,e,f,g = add m (t.left@t.right) (build_lists (m+1) q) in a,b,c,d,e,(t.rightcat.name,m)::(t.leftcat.name,m)::f,((match t.side with LEFT -> t.leftcat.name | RIGHT -> t.rightcat.name),m)::g

(** Performs 7 basic verifications on the grammar
 {ol
  {li The starting category symbols are used}
  {li The rules do not start with a licensor}
  {li A selecting feature corresponds to a defined category}
  {li A licensee has a defined corresponding licensor}
  {li A licensor has a defined corresponding licensee}
  {li A category is a starting category, is selected or is an adjoin to a defined category}
  {li The categories involved in an adjunction are defined}
 }
Warnings are printed out on the standard error channel if any error appears. You should consider that the output of the program is WRONG if you have any warning. Check your grammar if that occurs.
@return only the rules satisfying all these conditions. The verification is not recursive: if the rule defining a category is ignored, then the rules selecting this category are NOT ignored in general.
*)
let check (starts,rules) =

 (* Splits starting symbols in used and unused lists *)
 let rec starts_not_categories x = function
  [] -> [],[];
 | t::q when List.mem_assoc t x -> let (a,b)=(starts_not_categories x q) in (t::a,b)
 | t::q -> let (a,b) = (starts_not_categories x q) in (a,t::b) in

 let nr = List.length rules in

 let rulearray = Array.init nr (function a -> List.nth rules a) in
 (* the array of rules *)

 let mgarray = Array.init nr (function a -> to_string (List.nth rules a)) in
 (* the array with the mg form of the rule *)

 let rec init_list i nr = match (i,nr) with
  | a,b when a=b -> [a];
  | a,_ -> a::(init_list (a+1) nr) in

 let init_all = init_list 0 (nr-1) in

 let init_rules,_ (* init_adjun *) = List.partition (function n -> is_rule (rulearray.(n))) init_all in 

 let (categories,selectors,licensors,licensees,_ (* specialfeatures *) ,adjunctions,adjoins) = build_lists 0 rules in

 let warning filter init warn error_fun = 
   let errors = List.filter filter init in
   if errors <> [] then prerr_string warn;
   List.iter error_fun errors;
   errors in

 (* split starts between used and unused and print warnings for unused *)
 let (used_starts,unused_starts) = starts_not_categories categories starts in
 if unused_starts <> [] then prerr_string("\n/*** Warning: some start symbols are never used ***/\n");
 List.iter (function s -> prerr_string ("The starting symbol \""^s^"\" is never used as category in your grammar\n")) unused_starts;

 (* eliminates the rules where a licensor is the first feature and prints warnings *)
 let filter1 = function n -> is_lic (List.hd (features rulearray.(n)))  
 and warn1 = "\n/*** Warning: some rules have a licensor as first feature ***/\n"
 and error_fun1 = function n -> prerr_string("The rule \""^(mgarray.(n))^"\" (number "^string_of_int(n+1)^") has a licensor as first feature\n")
 in let errors1 = warning filter1 init_rules warn1 error_fun1 in

 (* a selecting feature has to select a defined category *)
 let filter2 = function (a,_)->not(List.mem_assoc a categories)
 and warn2 = "\n/*** Warning: some rules select non-existing categories ***/\n"
 and error_fun2 = function (s,n) ->  prerr_string("The rule \""^(mgarray.(n))^"\" (number "^string_of_int(n+1)^") selects an undefined \""^s^"\" category\n")
 in let errors2 = snd(List.split(warning filter2 selectors warn2 error_fun2)) in

 (* adjunctions use defined categories *)
 let filter2b = function (a,_) -> not(List.mem_assoc a categories)
 and warn2b = "\n/*** Warning: some rules select non-existing categories ***/\n"
 and error_fun2b = function (s,n) ->  prerr_string("The rule \""^(mgarray.(n))^"\" (number "^string_of_int(n+1)^") selects an undefined \""^s^"\" category\n")
 in let errors2b = snd(List.split(warning filter2b adjunctions warn2b error_fun2b)) in

 (* licensors and licensees are equal *)
 let filter3 = function x-> not(List.mem_assoc (fst(x)) licensees)
 and warn3 = "\n/*** Warning: some rules have orphan licensors ***/\n"
 and error_fun3 = function (s,n) -> prerr_string("The rule \""^(mgarray.(n))^"\" (number "^string_of_int(n+1)^") has the orphan licensor \""^s^"\"\n")
 in let errors3 = snd(List.split(warning filter3 licensors warn3 error_fun3)) in

 let filter4 = function x-> not(List.mem_assoc (fst(x)) licensors)
 and warn4 = "\n/*** Warning: some rules have orphan licensees ***/\n"
 and error_fun4 = function (s,n) -> prerr_string("The rule \""^(mgarray.(n))^"\" (number "^string_of_int(n+1)^") has the orphan licensee \""^s^"\"\n")
 in let errors4 = snd(List.split(warning filter4 licensees warn4 error_fun4)) in

 (* category never selected and not a starting category and not an adjoin *)
 let filter5 = function (a,_) -> not(List.mem_assoc a selectors || List.mem a used_starts || List.mem_assoc a adjoins)
 and warn5 = "\n/*** Warning: some categories are never selected nor starting categories nor adjoins ***/\n"
 and error_fun5 = function (s,n) -> prerr_string("The rule \""^(mgarray.(n))^"\" (number "^string_of_int(n+1)^") has a dead-end category \""^s^"\"\n")
 in let errors5 = snd(List.split(warning filter5 categories warn5 error_fun5)) in

 let remove l1 l2 = List.filter (function x -> not(List.mem x l2)) l1 in
 let final_rule_numbers = List.fold_left remove init_all [errors1;errors2;errors3;errors4;errors5;errors2b] in
 let final_rules = List.map (Array.get rulearray) final_rule_numbers in

 let rec split = function
   [] -> [],[]
 | (Rule t)::q -> let a,b= split q in (t::a,b)
 | (Adjunction t)::q -> let a,b = split q in (a,t::b) in
 let final_Rules,final_Adjunctions = split final_rules in

 used_starts,final_Rules,final_Adjunctions;;
 
