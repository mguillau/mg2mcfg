(** Implementation of dynamic arrays containing lists. It's therefore close
to hash-tables but resizing do not imply redistribution of the content. This entails that arrays can only grow, in order to not lose any element
and, in this implementation, there are no duplicate elements inside the lists.
@author Matthieu Guillaumin *) 

type 'a t = { mutable size: int;
	      mutable data: ('a list) array }

let create initial_size =
  let s = min (max 1 initial_size) Sys.max_array_length in
  { size = 0; data = Array.make s []}

let clear tbl =
  for i=0 to Array.length tbl.data - 1 do tbl.data.(i) <- [] done;
  tbl.size <- 0

let copy tbl =
  { size = tbl.size; data = Array.copy tbl.data}

let resize tbl n =
  let olddata = tbl.data in
  let oldsize = Array.length olddata in
  let newsize = min n Sys.max_array_length in
  if newsize <> oldsize then
    ( let newdata = Array.init newsize (function i -> if i<oldsize then olddata.(i) else [])
      in tbl.data <- newdata )

let add tbl key info =
  if key > Array.length tbl.data - 1 then resize tbl (2*key +1);
  let l = tbl.data.(key)
  in if not(List.mem info l) then (tbl.data.(key) <- info::l;
				   tbl.size <- succ tbl.size)

let find tbl key =
  if key > Array.length tbl.data - 1 then [] else tbl.data.(key)

let iter f tbl =
  for k=0 to Array.length tbl.data - 1 do
   List.iter (f k) tbl.data.(k)
  done
    
let fold f tbl init=
  let accu = ref init and d = tbl.data in
  for k = 0 to Array.length d - 1 do
   accu := List.fold_right (f k) d.(k) !accu
  done; !accu

let iter_key key f tbl = List.iter f (find tbl key)
			   
let fold_key key f tbl = List.fold_right f (find tbl key)
			   
let print_state out_ch tbl =
  let d = tbl.data in let n = Array.length d in
    output_string out_ch (string_of_int n^" rows: ");
    for k=0 to n-1 do
      let p = List.length tbl.data.(k) in
	output_string out_ch (string_of_int p^"-");
    done;
    
