{
(** Returns a stream of lexemes defined in mglexer.mll
	@author Matthieu Guillaumin
	@see 'mglexer.mll' for lexemes used *)
open Hmgparser;;
exception LexingError of int*string
}

rule token = parse
 [' ' '\t' '\n' '\r'] {token lexbuf}
| ( '/' [^'/']* '/' ) {token lexbuf}        (* ignoring things between '/'s *)
| ( '%' [^'\n']* '\n' )   {token lexbuf}    (* or behind a % is ignored *)
| ';' {ENDRULE}
| "::" {DEFINE}
| '=' {EQUAL}
| '+' {PLUS}
| '-' {MINUS}
| '>' {GREATER}
| '<' {SMALLER}
| (['a'-'z' '_' 'A'-'Z' '0'-'9'])+ {IDENT(Lexing.lexeme lexbuf)}
| ('\"' ([^'\"']* as idiom) '\"') {IDENT(idiom)}
| eof {ENDGRAMMAR}
| (_) {raise(LexingError(Lexing.lexeme_start lexbuf,Lexing.lexeme lexbuf))}
