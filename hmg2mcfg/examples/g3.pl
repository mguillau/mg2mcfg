%   File   : g3.pl
%   Author : E Stabler
%   Updated: Mar 00

[]::[='T','C'].
[]::[='Refl12',+k,'T'].         []::[='Acc3',+k,'T'].           []::[='Dat3',+k,'T'].      []::[=v,+k,'T'].
[se]::[='Acc3',+'F','Refl12'].  [se]::[='Dat3',+'F','Refl12'].  [se]::[=v,+'F','Refl12'].
[le]::[='Dat3',+'G','Acc3'].	[le]::[=v,+'G','Acc3'].
[lui]::[=v,+'F','Dat3'].
[]::[vacc<=,='D',v].            []::[vdat<=,='D',+k,vacc].      []::['V'<=,=p,vdat].       [montrera]::['V'].
[]::['P'<=,p].                  [a]::[='D',+k,'P'].             []::[p,-'F'].
['Jean']::['D',-k].             ['Marie']::['D',-k].
[le]::[='N','D',-k].            []::['D',-k,-'F'].              []::['D',-k,-'G'].
[roi]::['N'].                   [livre]::['N'].

startCategory('C').
