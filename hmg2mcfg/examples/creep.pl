%   File   : creep.pl
%   Author : W. Vermaat
%   Updated: 19-06-2001

% to derive ''Icarus uit Kreta weg zien vliegen''
% versus    '' ..     ..   ..  zien weg vliegen''

[weg] :: [adv, -a].
[weg] :: [adv].
[vliegen] :: [=adv, +a , ='D', v].
[vliegen] :: [=>adv, ='D', v].
['Icarus'] :: ['D'].
[zien] :: [<==v, 'V'].
[] :: [='V', 'C'].

startCategory('C').


