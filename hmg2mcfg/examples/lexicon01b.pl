%   File   : lexicon01.pl
%   Author : E Stabler
%   Updated: Mar 00

[]::[='V',+k,='D',+k,v].    []::[='V',='D',+k,v].
[died]::['V'].  [ate]::['V'].    [killed]::[='D','V'].     [ate]::[='D','V'].
[some]::[='N','D',-k].       [every]::[='N','D',-k].       [one]::[='N','D',-k].
[ant]::['N'].             [bee]::['N'].          [termite]::['N'].
[and]::[=v,=v,v].         [or]::[=v,=v,v].       [it,is,not,the,case,that]::[=v,v].

startCategory(v).
