%   File   : g0a.pl
%   Author : E Stabler
%   Updated: Mar 00

[] :: [='T','C'].                [] :: [='T',+wh,'C'].
                                 [] :: [='V',='D',v].
[] :: [=v,+k,'T'].               [] :: [=>'V',='D',v].
[like] :: [='D',+k,'V'].         [laugh] :: ['V'].
[the] :: [='N','D',-k].          [which] :: [='N','D',-k,-wh].
[king] :: ['N'].                 [pie] :: ['N'].

startCategory('C').
