%   File   : g-nilsen.pl
%   Author : E Stabler
%   Updated: Jun 01

[] :: [=bare,+adv,+top,'W'].
[] :: [='W',='Adv',+foc,bare].
[just]::['Adv',-adv].
[] :: [='Foc',+w,'W'].
[] :: [='Top',+v,'Foc',-foc].
[therefore] :: [='Fin','Top',-w,-top].
['-ed'] :: [='v',+i,'Fin'].    [] :: [='V',='D',='D','v',-v].    
[recognize] :: ['V',-i]. 
[one]::[='N','D'].  [student]::['N'].  [me]::['D'].

startCategory('C').
