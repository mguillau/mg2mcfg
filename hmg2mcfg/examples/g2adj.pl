%   File   : g2adj.pl via remnant movement
%   Author : E Stabler
%   Updated: Mar 00

startCategory('T').

['-s'] :: [v==>,+k,'T'].	% add just this for affix hopping

[] :: [=>'V',='D',v].
[eat] :: [='D',+k,'V'].        [eat] :: ['V'].               [laugh] :: ['V'].

[the] :: [='N','D',-k].

['Titus'] :: ['D',-k].

[king] :: ['N'].               [pie] :: ['N'].

[praise] :: [='D',+k,'V'].     [some] :: [='N','D',-k].

% modifiers
[v]<<['Adv'].  ['T']<<['Tmp'].  ['D',-case]<<['D',-case].
['Adv']>>[v].  ['Tmp']>>['T'].  [deg]>>['Adv'].

[happily]::['Adv'].  [sadly]::['Adv'].   [annually]::['Tmp'].   [daily]::['Tmp'].
[very]::[deg].  [so]::[deg].
