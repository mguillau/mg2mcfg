%   File   : g5.pl
%   Author : E Stabler
%   Updated: Oct 1999
%   Purpose: sample grammar for mgp. This one generates 1^n2^n3^n4^n5^n as an s

[] :: [s].               [] :: [=t1,+a,s].
[] :: [=t2,+b,t1].       [] :: [=t3,+c,t2].    [] :: [=t4,+d,t3].    [] :: [=a,+f,t4].
[1] :: [=b,a,-a].        [1] :: [=b,+a,a,-a].
[2] :: [=c,b,-b].        [2] :: [=c,+b,b,-b].
[3] :: [=d,c,-c].        [3] :: [=d,+c,c,-c].
[4] :: [=f,d,-d].        [4] :: [=f,+d,d,-d].
[5] :: [f,-f].           [5] :: [=a,+f,f,-f].

startCategory(s).
