%   File   : g-montreal.pl
%   Author : E Stabler
%   Updated: Oct 01

[] :: [='T','C'].               [] :: [='T',+'Wh','C'].
[] :: [=v,+'Case','T'].         [] :: [='V',+'Case',+'V',='D',v].
[makes] :: [='D','V',-'V'].
[maria] :: ['D',-'Case'].       [tortillas] :: ['D',-'Case'].
[what] :: ['D',-'Case',-'Wh'].

startCategory('C').
