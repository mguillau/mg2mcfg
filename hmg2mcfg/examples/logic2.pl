%   File   : logic.pl
%   Author : E Stabler
%   Updated: Mar 00

[a] :: ['E'].       [b] :: ['E'].      [x] :: ['E',-b].      [y] :: ['E',-b].
['P'] :: [='E','T'].   ['R'] :: [='E',='E','T'].
['-'] :: [='T','T'].
['&'] :: [='T',='T','T'].  [v] :: [='T',='T','T'].
['V'] :: [='T',+b,'T'].    ['E'] :: [='T',+b,'T'].

startCategory('T').
