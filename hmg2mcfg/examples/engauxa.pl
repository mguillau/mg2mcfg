%   File   : engauxa.pl via remnant movement
%   Author : E Stabler
%   Updated: Mar 00

[praise]::[='D','V',-i].    [criticize]::[='D','V',-i].
['Beatrice'] :: ['D',-case].   ['Benedick'] :: ['D',-case].
['-s']::[=v,+i,+case,'T'].   []::[='V',+case,='D',v].
[and]::[='T',='T','T'].

% only above items needed for "Beatrice criticize -s Benedick"

[the] :: [='N','D',-case].   [king] :: ['N'].               [pie] :: ['N'].
[eat]::[='D','V',-v].
[have]::[='PastPart',v,-v].
['-en']::[=v,+v,'PastPart',-aux].  
[be]::[='Prog',v,-v].
['-ing']::[=v,+v,'Prog',-aux].
[]::[=v,+aux,v].

% only above items needed for "the king have -s be -en eat -ing the pie"

[]::[=v,+v,'V[-fin]',-aux].
['-ed']::[=v,+v,'PastPart',-aux].
[laugh]::[='D','V',-v].
[]::[='V',v].
[will]::[='V[-fin]',v,-v].
['Lavinia']::['D',-case].      ['Titus']::['D',-case].

%[who]::['D',-case,-wh].
%[]::[='T','C'].
%[]::[='T',+wh,'C'].

startCategory('T').
