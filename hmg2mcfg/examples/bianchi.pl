%   File   : bianchi.pl
%   Author : John Hale
%   Updated: July 01
%  this file is intended to follow as closely as possible
%  Valentina Bianchi's account of Kayne's (Vergnaud's) raising analysis
%  of relative clauses under the `minimalist' alternative involving AgrP.
%
%  The example under study is her
%  (17)   the boy who I met.
%  from page 79 of "Consequences of Antisymmetry" (published version)
%  which is expands section 5.2 of her dissertation (p56)
%
%  Note: `the' and `boy' do not form a consituent
%  but rather the underlying form is 'the [CP I met who boy]'
%  in which [DP who boy] moves to specifier of CP to check a +rel feature.
%  Then `boy' moves to the specifier of a silent AgrD head, which itself
%  is incoporated with the external determiner `the' upon their merger.
%  This last phrasal movement is motivated by the need to check "strong nominal
%  features" on AgrD which I have simply coded as +nom. If there is
%  determiner agreement between `the' and `boy' in some language of the world,
%  maybe this feature should be called +phi. I have also completely ignored
%  case and tense in this grammar.


:- op(500, xfy, ::). % lexical items
:- op(500, fx, =). % for selection features
:- op(500, xf, <=). % for right incorporation
:- op(500, fx, =>). % for left incorporation

[] :: [=c,+nom,agrD].        % AgrP
[] :: [=i,+rel,c].           % CP
[the] :: [=>agrD,droot].     % the agrD head is to be incorporated on the left
['I'] :: [d].
[met] :: [=d,=d,i].
[who] :: [=n,d,-rel].
[boy] :: [n,-nom].

startCategory(droot).
