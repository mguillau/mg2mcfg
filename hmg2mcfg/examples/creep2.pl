%   File   : creep2.pl
%   Original: creep.pl
%   Author : W. Vermaat
%   Updated: 19-06-2001

% to derive ''Icarus uit Kreta weg zien vliegen''
% versus    '' ..     ..   ..  zien weg vliegen''
% with the use of functional categories

[weg] :: [adv, -a].
[weg] :: [adv].
[vliegen] :: [=>adv, ='D', v].
[vliegen] :: [=adv, ='D', v].
['Icarus'] :: ['D', -k].
[zien] :: [v<=, 'V'].

[] :: [='V', +k, 'I'].
[] :: [='V', +a, +k, 'I'].
[] :: [='I', 'C'].

startCategory('C').


