%   File   : hh.pl
%   Author : E Stabler
%   Updated: Aug 01
%   Purpose: This is grammar H, from Chap 6 of Harkema's thesis

:- op(500, xfy, ::). % lexical items
:- op(500, fx, =). % for selection features

[a] :: [b,-f].
[b] :: [=b,+f,b,-f].
[] :: [=b,+f,c].

startCategory(c).
