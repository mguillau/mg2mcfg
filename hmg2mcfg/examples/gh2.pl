%   File   : gh1.pl
%   Author : E Stabler
%   Updated: Feb 02

['-s'] :: [v==>,+k,'T'].	% for affix hopping

[] :: [='T','C'].              [] :: [=>'T','C'].            [] :: [=>'T',+wh,'C'].
['-s'] :: [=>'Modal',+k,'T'].  ['-s'] :: [=>'Have',+k,'T'].
['-s'] :: [=>'Be',+k,'T'].     ['-s'] :: [=v,+k,'T'].
[will] :: [='Have','Modal'].   [will] :: [='Be','Modal'].    [will] :: [=v,'Modal'].
[have] :: [='Been','Have'].    [have] :: [=ven,'Have'].
[be] :: [=ving,'Be'].          [been] :: [=ving,'Been'].
[] :: [=>'V',='D',v].          ['-en'] :: [=>'V',='D',ven].  ['-ing'] :: [=>'V',='D',ving].
[eat] :: [='D',+k,'V'].        [eat] :: ['V'].               [laugh] :: ['V'].

[the] :: [='Num','D',-k].        [which] :: [='Num','D',-k,-wh].
['-s'] :: ['N'==>,'Num'].       [] :: [='N','Num'].

[king] :: ['N'].               [pie] :: ['N'].

[praise] :: [='D',+k,'V'].     [some] :: [='N','D',-k].

startCategory('C').
