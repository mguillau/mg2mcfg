%   File   : eng.pl via remnant movement
%   Author : E Stabler
%   Updated: Mar 00

[praise]::[='D','V',-i].    [criticize]::[='D','V',-i].
['Beatrice'] :: ['D',-case].   ['Benedick'] :: ['D',-case].
['-s']::[=v,+i,+case,'T'].   []::[='V',+case,='D',v].
[and]::[='T',='T','T'].

% only above items needed for "Beatrice criticize -s Benedick"

startCategory('T').
