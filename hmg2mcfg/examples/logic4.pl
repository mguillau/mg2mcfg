%   File   : logic.pl
%   Author : E Stabler
%   Updated: Mar 00

[a] :: ['E'].       [b] :: ['E'].      [x] :: ['E',-x].      [y] :: ['E',-y].
['P'] :: [='E','T'].   ['R'] :: [='E',='E','T'].
['-'] :: [='T','T'].
['&'] :: [='T',='T','T'].  [v] :: [='T',='T','T'].
[] :: [='T',+x,='Q','T'].  [] :: [='T',+y,='Q','T'].
['E'] :: ['Q'].     ['V'] :: ['Q'].

startCategory('T').
