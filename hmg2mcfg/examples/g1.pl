%   File   : g1.pl
%   Author : E Stabler
%   Updated: Mar 00

[] :: [='T','C'].              [] :: [=>'T','C'].            [] :: [=>'T',+wh,'C'].
['-s'] :: [=>'Modal',+k,'T'].  ['-s'] :: [=>'Have',+k,'T'].
['-s'] :: [=>'Be',+k,'T'].     ['-s'] :: [=v,+k,'T'].
[will] :: [='Have','Modal'].   [will] :: [='Be','Modal'].    [will] :: [=v,'Modal'].
[have] :: [='Been','Have'].    [have] :: [=ven,'Have'].
[be] :: [=ving,'Be'].          [been] :: [=ving,'Been'].
[] :: [=>'V',='D',v].          ['-en'] :: [=>'V',='D',ven].  ['-ing'] :: [=>'V',='D',ving].
[eat] :: [='D',+k,'V'].        [eat] :: ['V'].               [laugh] :: ['V'].
[the] :: [='N','D',-k].        [which] :: [='N','D',-k,-wh].
[king] :: ['N'].               [pie] :: ['N'].

startCategory('C').
