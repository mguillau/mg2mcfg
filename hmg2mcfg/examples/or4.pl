%   File   : or4.pl
%   Author : E Stabler
%   Updated: Feb 2002
%  simple orthography to go with gh4.pl

orth(does,['-s']).
orth(will,[will,'-s']).
orth(has,[have,'-s']).      
orth(have,[have,'-s']).
orth(had,[have,'-en']).
orth(having,[have,'-ing']).
orth(am,[be,'-s']).
orth(are,[be,'-s']).
orth(is,[be,'-s']).
orth(being,[be,'-ing']).
orth(praise,[praise,'-s']).
orth(praises,[praise,'-s']).
orth(praised,[praise,'-en']).
orth(praising,[praise,'-ing']).
orth(eat,[eat,'-s']).
orth(eats,[eat,'-s']).
orth(eaten,[eat,'-en']).
orth(eating,[eat,'-ing']).
orth(charge,[charge,'-s']).
orth(charges,[charge,'-s']).
orth(charged,[charge,'-en']).
orth(charging,[charge,'-ing']).
orth(laugh,[laugh,'-s']).
orth(laughs,[laugh,'-s']).
orth(laughed,[laugh,'-en']).
orth(laughing,[laugh,'-ing']).
orth(know,[know,'-s']).
orth(knows,[know,'-s']).
orth(known,[know,'-en']).
orth(knowing,[know,'-ing']).
orth(doubt,[doubt,'-s']).
orth(doubts,[doubt,'-s']).
orth(doubted,[doubt,'-en']).
orth(doubting,[doubt,'-ing']).
orth(think,[think,'-s']).
orth(thinks,[think,'-s']).
orth(thought,[think,'-en']).
orth(thinking,[think,'-ing']).
orth(wonder,[wonder,'-s']).
orth(wonders,[wonder,'-s']).
orth(wondered,[wonder,'-en']).
orth(wondering,[wonder,'-ing']).
orth(seem,[seem,'-s']).
orth(seems,[seem,'-s']).
orth(prefer,[prefer,'-s']).
orth(prefers,[prefer,'-s']).
orth(preferred,[prefer,'-en']).
orth(prefering,[prefer,'-ing']).
orth(consider,[consider,'-s']).
orth(considers,[consider,'-s']).
orth(considered,[consider,'-en']).
orth(considering,[consider,'-ing']).
orth(try,[try,'-s']).
orth(tries,[try,'-s']).
orth(tried,[try,'-en']).
orth(trying,[try,'-ing']).
orth(want,[want,'-s']).
orth(wants,[want,'-s']).
orth(wanted,[want,'-en']).
orth(wanting,[want,'-ing']).

orth(kings,[king,'-s']).
orth(pies,[pie,'-s']).
orth(apples,[apple,'-s']).
orth(human,[human,'-s']).
orth(car,[car,'-s']).
orth(shirt,[shirt,'-s']).
orth(language,[language,'-s']).
orth(claim,[claim,'-s']).
orth(proposition,[proposition,'-s']).
orth(student,[student,'-s']).
orth(citizen,[citizen,'-s']).

orth(an,[a]).

orth(bartend,[bar,tend]).
orth(bartends,[bar,tend,'-s']).
