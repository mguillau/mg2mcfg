%   File   : gpp.pl
%   Author : E Stabler
%   Updated: June 01

:- op(500, xfy, ::). % lexical items
:- op(500, fx, =). % for selection features

[]::[='V',='D','S'].
['Titus']::['D']. ['Tamora']::['D'].  ['Rome']::['D']. ['Italy']::['D'].
[the]::[='N','D'].
[queen]::['N']. []::[='P',='N','N'].
[enslaved]::[='D','V']. []::[='P',='V','V'].
[in]::[='D','P'].

startCategory('S').
