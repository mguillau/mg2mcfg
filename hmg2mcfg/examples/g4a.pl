%   File   : g4a.pl
%   Author : E Stabler
%   Updated: Jun 01
%      grammar for the language {aXYb| X\in{0,1}^n,Y=X+2^n,n>=0}
%      -- a TAG language, similar to the copy language grammar in 
%      Keenan & Stabler 2001, but rigid

[] :: ['T'].
[a] :: ['C',-r,-l].           [b] :: [='C',+r,+l,'T'].
[2] :: [='C',+r,'A',-r].      [3] :: [='C',+r,'B',-r].
[0] :: [='A',+l,'C',-l].      [1] :: [='B',+l,'C',-l].

startCategory('T').
