%   File   : ghrigid.pl
%   Author : E Stabler
%   Updated: Feb 2002

% complementizers
[that]::[='T','Ce'].
[]::[='T','C'].         []::[=>'T','C'].        []::[=>'T',+wh,'C'].    []::[='T',+wh,'C'].
[]::[='T',+wh,'Cwh'].   []::[=>'T',+wh,'Cwh'].  % embedded wh-clause

% finite tense
['-s']::[v==>,+k,'T'].	% for affix hopping
['-s']::[=>'Modal',+k,'T'].  ['-s']::[=>'Have',+k,'T'].   ['-s']::[=>'Be',+k,'T'].     ['-s']::[=v,+k,'T'].

% determiners
[the]::[='N','D',-k].        [every]::[='N','D',-k].    [a]::[='N','D',-k].
[some]::[='N','D',-k].       [some]::['D',-k].

% number marking (singular, plural)
[]::[='N','Num'].              ['-s']::['N'==>,'Num'].

% simple nouns
[process]::['N'].       [user]::['N'].                    [machine]::['N'].
[network]::['N'].       [robot]::['N'].                   [file]::['N'].
[directory]::['N'].     [action]::['N'].                  [virus]::['N'].
[intruder]::['N'].      [agent]::['N'].                   [worm]::['N'].

% names as lexical DPs
[collier]::['D',-k].     [kobele]::['D',-k].       [stabler]::['D',-k].      [taylor]::['D',-k].
[taylor0]::['D',-k].     [wayrachaki]::['D',-k].   [wintermute]::['D',-k].   [kallpa]::['D',-k].
[wiener]::['D',-k].      [lerner]::['D',-k].       [protos]::['D',-k].       [mail0]::['D',-k].
[rdist]::['D',-k].       [fingerd]::['D',-k].      [ftp]::['D',-k].          [telnet]::['D',-k].
['biology.ucla.edu']::['D',-k].  ['ling.ucla.edu']::['D',-k].  ['alife.org']::['D',-k]. 

% pronouns as lexical determiners
[she]::['D',-k].  [he]::['D',-k].   [it]::['D',-k].  ['I']::['D',-k]. [you]::['D',-k]. [they]::['D',-k].  % nom
[her]::['D',-k].  [him]::['D',-k].                   [me]::['D',-k].  [us]::['D',-k].  [them]::['D',-k].  % acc
[my]::[='Num','D',-k]. [your]::[='Num','D',-k].
[her]::[='Num','D',-k]. [his]::[='Num','D',-k].  [its]::[='Num','D',-k].  % gen

% wh determiners
[which]::[='N','D',-k,-wh].  [which]::['D',-k,-wh].
[what]::[='N','D',-k,-wh].   [what]::['D',-k,-wh].

% auxiliary verbs: problematic in rigid grammar
[will]::[='Have','Modal'].   [will]::[='Be','Modal'].    [will]::[=v,'Modal'].
[have]::[='Been','Have'].    [have]::[=ven,'Have'].
[be]::[=ving,'Be'].          [been]::[=ving,'Been'].

% little v
[]::[=>'V',='D',v].
['-en']::[=>'V',='D',ven].  ['-ing']::[=>'V',='D',ving].
['-en']::[=>'V',ven].       ['-ing']::[=>'V',ving].

% DP-selecting (transitive) verbs - select an object, and take a subject too (via v)
[start]::[='D',+k,'V'].   [modify]::[='D',+k,'V'].  [destroy]::[='D',+k,'V'].  [create]::[='D',+k,'V'].

% intransitive verbs - select no object, but take a subject
[crash]::['V'].          [sing]::['V'].       [charge]::['V'].    [eat]::['V'].

% CP-selecting verbs
[know]::[='Ce','V'].  %   [know]::[='D',+k,'V'].   [know]::['V'].
[doubt]::[='Ce','V']. %   [doubt]::[='Cwh','V'].   [doubt]::[='D',+k,'V'].  [doubt]::['V'].
[think]::[='Ce','V']. %                                                    [think]::['V'].
                      %  [wonder]::[='Cwh','V'].                           [wonder]::['V'].
% CP-selecting nouns
[claim]::[='Ce','N']. %  [proposition]::[='Ce','N'].  [claim]::['N'].      [proposition]::['N'].

% raising verbs - select only propositional complement, no object or subject
[seem]::[='T',v].

% infinitival tense 
[to]::[=v,'T'].     [to]::[='Have','T'].        [to]::[='Be','T'].   % nb does not select modals

% little a
[]::[=>'A',='D',a].

% simple adjectives
[black]::['A'].  [white]::['A'].       [human]::['A'].  [mortal]::['A'].
[happy]::['A'].  [unhappy]::['A'].

% verbs with AP complements: predicative be, seem
%[be]::[=a,'Be'].    [seem]::[=a,v].
[be]::[='A','Be'].    [seem]::[='A',v].

% adjectives with complements
[proud]::[=p,'A'].   [proud]::['A'].   [proud]::[='T',a].

% little p (no subject?)
[]::[=>'P',p].

% prepositions with no subject
[of]::[='D',+k,'P'].        [about]::[='D',+k,'P'].        [on]::[='D',+k,'P'].

% verbs with AP,TP complements: small clause selectors as raising to object
[prefer]::[=a,+k,'V'].      %[prefer]::[='T',+k,'V'].     [prefer]::[='D',+k,'V'].
[consider]::[=a,+k,'V'].    %[consider]::[='T',+k,'V'].   [consider]::[='D',+k,'V'].

% nouns with PP complements
%[student]::[=p,'N'].        [student]::['N'].
%[citizen]::[=p,'N'].        [citizen]::['N'].

% more verbs with PP complements
%[be]::[=p,v].    [seem]::[=p,v].
%[]::[=>'P',='D',p].
[up]::[='D',+k,'P'].
[creek]::['N'].

% control verbs
[try]::[='T','V'].
[want]::[='T','V'].      [want]::[='T',+k,'V'].

% verbs with causative alternation: using little v that does not select subject
[break]::[='D',+k,'V']. 
% one idea, but this intrans use of the transitivizer v can cause trouble:
%[break]::[='D','V'].  []::[=>'V',v]. 
% so, better:
%[break]::[='D',v]. 

% simple idea about PRO that does not work: []::['D'].
% second idea: "null case" feature k0
%[]::['D',-k0].
%[to]::[=v,+k0,'T'].     [to]::[='Have',+k0,'T'].        [to]::[='Be',+k0,'T'].   % nb does not select modals

% Boolean operators: problematic in rigid grammar

% modifiers:
['N']<<[p].    %[v]<<[p].      [v]<<['Adv'].  ['D',-k]<<['D',-k].   %['N']<<['A']. % for testing only
['A']>>['N'].  %['Adv']>>[v].  [deg]>>['A'].  [deg]>>['Adv'].  ['Adv']>>['P'].   [emph]>>['D',-k].  [qu]>>['Num'].

[completely]::['Adv'].  [happily]::['Adv'].  [very]::[deg].   [only]::[emph].   [3]::[qu].

startCategory('T').
