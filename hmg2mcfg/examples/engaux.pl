%   File   : engaux.pl via remnant movement
%   Author : E Stabler
%   Updated: Mar 00

[love]::[='D','V',-v].
['Romeo'] :: ['D',-case].   ['Juliet'] :: ['D',-case].
['-s']::[=v,+v,+case,'T'].   []::[='V',+case,='D',v].

% only above items needed for "Romeo love -s Juliet"

[the] :: [='N','D',-case].   [king] :: ['N'].               [pie] :: ['N'].
[eat]::[='D','V',-v].
[have]::[='PastPart',v,-v].
['-en']::[=v,+v,'PastPart',-aux].  
[be]::[='Prog',v,-v].
['-ing']::[=v,+v,'Prog',-aux].
[]::[=v,+aux,v].

% only above items needed for "the king have -s be -en eat -ing the pie"

[]::[=v,+v,'V[-fin]',-aux].
['-ed']::[=v,+v,'PastPart',-aux].
[laugh]::[='D','V',-v].
[]::[='V',v].
[will]::[='V[-fin]',v,-v].
['Lavinia']::['D',-case].      ['Titus']::['D',-case].
[]::[='T','C'].
[]::[='T',+wh,'C'].
[who]::['D',-case,-wh].

startCategory('T').
