%   File   : g-nilsen2.pl
%   Author : E Stabler
%   Updated: Jun 01


%[] :: [=bare,+adv,+top,'W'].
%[] :: [='W',='Adv',+foc,bare].
%[just]::['Adv',-adv].
%[] :: [='Foc',+w,'W'].
%[] :: [='Top',+v,'Foc',-foc].
%[therefore] :: [='Fin','Top',-w,-top].

[usually]::[='W',+top,'Adv'].
[] :: [='bare',+fpt,+w,'W'].
[] :: [='Neg',=fpt,+fin,bare].
[just]::[fpt,-fpt].
[not]::[='Top',+top,'Neg'].
[]::[='Fin',+top,'Top',-top,-w,-top].
['-s'] :: [='v',+i,'Fin',-fin].    [] :: [='V',='D','v'].
[answer] :: ['V',-i]. 
['John']::['D',-top].

startCategory('C').
