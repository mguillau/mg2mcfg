%   File   : engaux3.pl
%   Author : E Stabler
%   Updated: Mar 00

['-s'] :: [=>'Have',+case,'T'].
[have] :: [=ven,'Have'].
['-en'] :: [=>'Be',='D',ven].
[eat] :: [='D',+case,'V'].
[the] :: [='N','D',-case].
[pie] :: ['N'].
[king] :: ['N'].
[be] :: [=ving,'Be'].
['-ing'] :: [=>'V',ving].

%['-en'] :: [=>'V',='D',ven].
%['-ing'] :: [=>'V',='D',ving].
%['-s'] :: [=>'Be',+case,'T'].
%[] :: [=>'V',='D',v].
%[have] :: [='Been','Have'].
%[been] :: [=ving,'Been'].
%['-s'] :: [=v,+case,'T'].
%['-s'] :: [=>'Modal',+case,'T'].
%[will] :: [='Have','Modal'].   [will] :: [='Be','Modal'].    [will] :: [=v,'Modal'].
%[eat] :: ['V'].               [laugh] :: ['V'].
%[] :: [='T','C'].              [] :: [=>'T','C'].            [] :: [=>'T',+wh,'C'].
%[which] :: [='N','D',-case,-wh].

startCategory('T').
