% File   : onesentpros.pl
% Author : L Buell
% Updated: May 2000
% Purpose: Swahili declaratives

([Word] :: Features) :- lex(Word, Features, _Prosody).
([] :: Features) :- lexempty(Features, _Prosody).

% Issues:
%  - Features are privative.
%  - Lexempty items have been given a prosodic feature list.
%    This contains the one prosodic feature "0" (zero syllables).
%  - Some "func" features may not be utilized, so it might
%    be better not to use features as resources.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
%                       T E S T
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Merge a complement that won't move: r1
%lex( r1_head, [ =c, h ], [ 1, root] ).
%lex( r1_comp, [ c ], [ 2, suffix ] ).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
%                       V E R B S
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
lex( soma, [ =dobj, =d, v ], [ 2, root ]  ).
lex( soma, [ =dobj, =d, =adv, v ], [ 2, root ] ).
lex( nunua, [ =dobj, =d, v ], [ 3, root ]  ).
lex( nunua, [ =dobj, =d, =adv, v ], [ 3, root ] ).
lex( ona, [ =dobj, =d, v ], [ 2, root ]  ).
lex( ona, [ =dobj, =d, =adv, v ], [ 2, root ] ).
lex( la, [ =dobj, =d, v ], [ 1, root ]  ).
lex( la, [ =dobj, =d, =adv, v ], [ 1, root ] ).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                   A D V E R B S
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
lexempty( [ =p, adv ], [ 0 ] ).
lexempty( [ =p, adv, -adv ], [ 0 ] ).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                       N O U N S
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
lex( gazeti, [ n5 ], [ 3, root ] ).
lex( kitabu, [ n7 ], [ 3, root ] ).
lex( kitunguu, [ n7 ], [ 4, root ] ).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%           D E T E R M I N E R   P H R A S E S
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
lexempty( [ d, -subj, -fpssubj ], [ 0 ] ).
lexempty( [ d, -subj, -spssubj ], [ 0 ] ).
lexempty( [ =n5, dobj, -n5obj, -obj ], [ 0 ] ).
lexempty( [ =n7, dobj, -n7obj, -obj ], [ 0 ] ).
lexempty( [ dobj, -fpsobj, -obj ], [ 0 ] ).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                     C L I T I C S
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
lexempty( [ =v, +n5obj, cl, -cl ], [ 0 ] ).
lex( li, [ =v, +n5obj, cl, -cl ], [ 1, prefix ] ).
lexempty( [ =v, +n7obj, cl, -cl ], [ 0, prefix ] ).
lex( ki, [ =v, +n7obj, cl, -cl ], [ 1, prefix ] ).
lex( ni, [ =v, +fpsobj, cl, -cl ], [ 1, prefix ] ).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                     V P   C A P S
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
lexempty( [=cl, +adv, w], [ 0 ] ).
lexempty( [=cl, w], [ 0 ] ).
lexempty( [=w, +obj, x], [ 0 ] ).
lexempty( [=x, +subj, y], [ 0 ] ).
lexempty( [=y, +cl, z], [ 0 ] ).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                     I P   R E G I O N
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
lex( li, [=z, aux], [ 1, root, func ] ).
lex( na, [=z, aux], [ 1, root, func ] ).
lex( a, [=z, aux], [ 1, func ] ).
lex( ka, [=z, aux], [ 1, func ] ).

lex( ni, [=aux, +fpssubj, agrs], [ 1, prefix, func ] ).
lex( u, [=aux, +spssubj, agrs], [ 1, prefix, func ] ).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                     S T A R T
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
startCategory( agrs ).

