%   File   : bit1.pl
%   Author : E Stabler
%   Updated: Sep 02
% purpose: tiny examples

%bit1a:
[a] :: ['A'].
[] :: ['B'].
[c] :: ['C',-(1)].
[t] :: [='A',='B',='C',+(1),'T'].

%bit1b:
%[a] :: ['A'].
%[] :: ['B',-(1)].
%[c] :: ['C'].
%[t] :: [='A',='B',='C',+(1),'T'].

startCategory('T').
