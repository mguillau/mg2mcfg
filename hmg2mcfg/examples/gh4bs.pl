%   File   : gh4bs.pl
%   Author : E Stabler
%   Updated: Feb 2002
% This is gh4 modified for the Beghelli & Stowell proposal

% complementizers
[] :: [='T','C'].         [] :: [=>'T','C'].     [] :: [=>'T',+wh,'C'].  [] :: [='T',+wh,'C'].
[that] :: [='T','Ce'].    [] :: [='T','Ce'].     [] :: [='T',+wh,'Cwh'].   % embedded clauses
% new complementizers
[]::[='T',+group,'C'].  []::[=>'T',+group,'C'].       []::[=>'T',+wh,+group,'C'].   []::[='T',+wh,+group,'C'].

% finite tense
['-s'] :: [v==>,+k,'T'].	% for affix hopping
['-s'] :: [=>'Modal',+k,'T'].  ['-s'] :: [=>'Have',+k,'T'].   ['-s'] :: [=>'Be',+k,'T'].     ['-s'] :: [=v,+k,'T'].
% new finite tense -- notice that hopping down across Dist, Share, Neg to v is not possible
[]::[=v,+k,'T'].       []::[=v,+k,+count,'T'].       []::[='Dist',+k,'T'].    []::[='Dist',+k,+count,'T'].
[]::['Share',+k,'T'].  []::[='Share',+k,+count,'T']. []::[ ='Neg',+k,'T'].    []::[='Neg',+k,+count,'T'].
[]::[=v,+k,'T'].       []::[=v,+k,+count,'T'].

[]::[='Share',+dist,'Dist'].  []::[='Neg',+dist,'Dist'].    []::[=v,+dist,'Dist'].
                              []::[='Neg',+group,'Share'].  []::[=v,+group,'Share']
                                                            []::[=v,+neg,'Neg'].
% determiners
[which] :: [='Num','D',-k,-wh].  [which] :: ['D',-k,-wh].
[what] :: [='Num','D',-k,-wh].   [what] :: ['D',-k,-wh].

no::[='N','D',-k,-neg].      every::[='N','D',-k,-dist].    few::[='N','D',-k,-count].
the::[='N','D',-k,-group].   some::[='N','D',-k,-group].    one::[='N','D',-k,-group].   two::[='N','D',-k,-group].
the::[='N','D',-k].          some::[='N','D',-k].           one::[='N','D',-k].          two::[='N','D',-k].

% simple nouns
[king] :: ['N'].               [pie] :: ['N'].
[coffee] :: ['N'].             [shirt] :: ['N'].         [language] :: ['N'].

% number marking (singular, plural)
[] :: [='N','Num'].              ['-s'] :: ['N'==>,'Num'].

% names as lexical DPs
['Titus'] :: ['D',-k].         ['Lavinia'] :: ['D',-k].      ['Tamara'] :: ['D',-k].

% pronouns as lexical determiners
[she] :: ['D',-k].             [he] :: ['D',-k].             [it] :: ['D',-k]. % nominative
[her] :: [='Num','D',-k].      [his] :: [='Num','D',-k].     [its] :: [='Num','D',-k].  % genitive

% auxiliary verbs
[will] :: [='Have','Modal'].   [will] :: [='Be','Modal'].    [will] :: [=v,'Modal'].
[have] :: [='Been','Have'].    [have] :: [=ven,'Have'].
[be] :: [=ving,'Be'].          [been] :: [=ving,'Been'].

% little v
[] :: [=>'V',='D',v].          ['-en'] :: [=>'V',='D',ven].  ['-ing'] :: [=>'V',='D',ving].

% DP-selecting (transitive) verbs - select an object, and take a subject too (via v)
[eat] :: [='D',+k,'V'].        [praise] :: [='D',+k,'V'].

% intransitive verbs - select no object, but take a subject
[eat] :: ['V'].                [laugh] :: ['V'].

% CP-selecting verbs
[know] :: [='Ce','V'].    [know] :: [='Cwh','V'].    [know] :: [='D',+k,'V'].   [know] :: ['V'].
[doubt] :: [='Ce','V'].   [doubt] :: [='Cwh','V'].   [doubt] :: [='D',+k,'V'].  [doubt] :: ['V'].
[think] :: [='Ce','V'].                                                         [think] :: ['V'].
                          [wonder] :: [='Cwh','V'].                             [wonder] :: ['V'].

% CP-selecting nouns
[claim] :: [='Ce','N'].         [proposition] :: [='Ce','N'].

% raising verbs - select only propositional complement, no object or subject
[seem] :: [='T',v].

% infinitival tense 
[to] :: [=v,'T'].     [to] :: [='Have','T'].        [to] :: [='Be','T'].   % nb does not select modals

% little a
[] :: [=>'A',='D',a].

% simple adjectives
[black] :: ['A'].  [white] :: ['A'].
[happy] :: ['A'].  [unhappy] :: ['A'].

% verbs with AP complements: predicative be, seem
[be] :: [=a,'Be'].    [seem] :: [=a,v].

% adjectives with complements
[proud] :: [=p,'A'].   [proud] :: ['A'].   [proud] :: [='T',a].

% little p (no subject?)
[] :: [=>'P',p].

% prepositions with no subject
[of] :: [='D',+k,'P'].            [about] :: [='D',+k,'P'].

% verbs with AP,TP complements: small clause selectors as raising to object
[prefer] :: [=a,+k,'V'].      [prefer] :: [='T',+k,'V'].
[consider] :: [=a,+k,'V'].    [consider] :: [='T',+k,'V'].

% nouns with PP complements
[student] :: [=p,'N'].        [student] :: ['N'].
[citizen] :: [=p,'N'].        [citizen] :: ['N'].

% more verbs with PP complements
[be] :: [=p,v].    [seem] :: [=p,v].
[] :: [=>'P',='D',p].  [up] :: [='D',+k,'P'].
[creek] :: ['N'].

% control verbs
[try] :: [='T','V'].
[want] :: [='T','V'].      [want] :: [='T',+k,'V'].

% simple idea about PRO that does not work: [] :: ['D'].
% second idea:
[] :: ['D',-k0].
[to] :: [=v,+k0,'T'].     [to] :: [='Have',+k0,'T'].        [to] :: [='Be',+k0,'T'].   % nb does not select modals

startCategory('C').
