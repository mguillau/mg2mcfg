%   File   : lexicon01c.pl
%   Author : E Stabler
%   Updated: Mar 00

[]::[='V',+k,='D',+k,+i,v].    []::[='V',='D',+k,+i,v].
[died]::['V'].    [ate]::['V'].     [killed]::[='D','V',-i].     [ate]::[='D','V',-i].
[some]::[='N','D',-k].       [every]::[='N','D',-k].       [one]::[='N','D',-k].
[ant]::['N'].             [insect]::['N'].          [termite]::['N'].
[and]::[=v,=v,v].         [or]::[=v,=v,v].       [it,is,not,the,case,that]::[=v,v].

startCategory(v).
