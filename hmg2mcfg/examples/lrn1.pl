%   File   : lrn1.pl
%   Author : E Stabler
%   Updated: Nov 01

:- op(500, xfy, ::). % lexical items
:- op(500, fx, =). % for selection features

[a] :: [='A','C'].              [b] :: ['C'].

startCategory('C').
