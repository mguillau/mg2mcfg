%   File   : ne.pl - naive english
%   Author : E Stabler
%   Updated: Mar 00

[] :: [=i,c].
['-s'] :: [=pred,+v,+d,i].
[] :: [=vt,=d,=d,pred].       [] :: [=v,=d,pred].
[praise] :: [vt,-v].          [laugh[ :: [v,-v].
[lavinia] :: [d].             [titus] :: [d].
[lavinia] :: [d,-d].          [titus] :: [d,-d].

startCategory(c).
