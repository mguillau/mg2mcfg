%   File   : logic.pl
%   Author : E Stabler
%   Updated: Mar 00

[a] :: ['E'].       [b] :: ['E'].      [x] :: ['E',-x].      [y] :: ['E',-y].
['P'] :: [='E','T'].   ['R'] :: [='E',='E','T'].
['-'] :: [='T','T'].
['&'] :: [='T',='T','T'].  [v] :: [='T',='T','T'].
['V'] :: [='T',+x,'T'].    ['V'] :: [='T',+y,'T'].
['E'] :: [='T',+x,'T'].    ['E'] :: [='T',+y,'T'].

startCategory('T').
