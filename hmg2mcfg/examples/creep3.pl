%   File   : creep3.pl
%   Original: creep.pl
%   Author : W. Vermaat
%   Updated: 19-06-2001

% to derive (de zon waar Icarus) 
% ''door kon gaan dreigen te worden getroffen''
% ''door kon gaan dreigen getroffen te worden ''
% ''door kon gaan getroffen dreigen te worden ''
% ''door kon getroffen gaan dreigen te worden ''
% ''door getroffen kon gaan dreigen te worden ''

[] :: [='I', 'C'].
[] :: [='I', +i, +k, 'C'].
[] :: [=fin, 'I'].
[] :: [=inf, 'I'].
[] :: [=aux, 'I'].
[ge]::['V'<=,'X',-a].
[ge]::['V'<=, 'X'].
[troffen]::['V', -v].
[worden]::[='X' ,aux].
[worden]::[='X',+v,aux].
[worden]::[='X',+a, +v, aux].
[dreigen]::[=aux, inf].
[dreigen]::[=aux, +a, +v, inf].
[kon]::[=inf, fin].
[kon]::[=inf, +a, +v, fin].

[troffen]::[=d, =p, 'V', -v].
[icarus]:: [d, -k].
[door]:: [p, -i].

startCategory('C').

