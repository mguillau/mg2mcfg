%   File   : engaux2.pl
%   Author : E Stabler
%   Updated: Mar 00

['Romeo'] :: ['D',-case].  ['Juliet'] :: ['D',-case].
[love] :: [='D','V'].
[eat] :: [='D','V'].

% v has two specs:
['-s'] :: [=>v,+case,'T']. [] :: [=>'V',+case,='D',v].
% or else, v with one specs selecting case assigner
%[] :: [=>'V',+case,'Acc'].  [] :: [=>'Acc',='D',v].  ['-s'] :: [=>v,+case,'T'].

% only above items needed for "Romeo love -s Juliet"

%[] :: [='T','C'].              [] :: [=>'T','C'].            [] :: [=>'T',+wh,'C'].
['-s'] :: [=>'Modal',+case,'T'].  ['-s'] :: [=>'Have',+case,'T'].
['-s'] :: [=>'Be',+case,'T'].     ['-s'] :: [=v,+case,'T'].
[will] :: [='Have','Modal'].   [will] :: [='Be','Modal'].    [will] :: [=v,'Modal'].
[have] :: [='Been','Have'].    [have] :: [=ven,'Have'].
[be] :: [=ving,'Be'].          [been] :: [=ving,'Been'].
[the] :: [='N','D',-case].        [which] :: [='N','D',-case,-wh].
[king] :: ['N'].               [pie] :: ['N'].

%[] :: [=>'V',+case,='D',v].       ['-en'] :: [=>'V',+case,='D',ven].  ['-ing'] :: [=>'V',+case,='D',ving].
%[eat] :: [='D','V'].        [eat] :: ['V'].               [laugh] :: ['V'].

[] :: [=>'V',='D',v].       ['-en'] :: [=>'V',='D',ven].  ['-ing'] :: [=>'V',='D',ving].
[eat] :: [='D',+case,'V'].        [eat] :: ['V'].               [laugh] :: ['V'].

startCategory('T').
