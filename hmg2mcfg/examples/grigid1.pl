%   File   : grigid1.pl
%   Author : E Stabler
%   Updated: jun 01

[criticizes] :: [='D',='D','V'].  [praises] :: [='D',='D','V'].
[believes] :: [='C',='D','V'].  [doubts] :: [='C',='D','V'].
[laughs] :: [='D','V'].  [cries] :: [='D','V'].
['Pierre'] :: ['D'].  ['Marie'] :: ['D'].   [what] :: ['D',-wh].
[teacher] :: ['N'].    [student] :: ['N'].
[the] :: [='N','D'].  [some] :: [='N','D'].
[]::[='V',+wh,'V'].  [that] :: [='V','C'].

%[sees] :: [='V',='D','V'].
%[idea] :: [='C','N'].  

startCategory('V').
