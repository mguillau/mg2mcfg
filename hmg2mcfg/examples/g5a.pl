%   File   : g5a.pl
%   Author : E Stabler
%   Updated: Jun 2001
%   Purpose: sample grammar for a1^nvb2^nzc3^nyd4^nxe5^nw -- not a TAG language
%     (similar to the 5 counting dependency grammar in Stabler 1999, but rigid)

[a]::[='B','A',-a].   [b]::[='C','B',-b].  [c]::[='D','C',-c].  [d]::[='E','D',-d].  [e]::['E',-e].
[1]::[=2,+a,'A',-a].  [2]::[=3,+b,2,-b].   [3]::[=4,+c,3,-c].   [4]::[=5,+d,4,-d].   [5]::[='A',+e,5,-e].
[v]::[='Z',+a,'T'].   [w]::[='A',+e,'W'].  [x]::[='W',+d,k].    [y]::[=k,+c,'Y'].    [z]::[='Y',+b,'Z'].

startCategory('T').
