%   File   : gpc.pl
%   Author : E Stabler
%   Updated: July 01

:- op(500, xfy, ::). % lexical items
:- op(500, fx, =). % for selection features

[p] :: ['S'].    [q] :: ['S'].    [r] :: ['S'].
['-'] :: [='S','S'].    ['&'] :: [='S',='S','S'].    ['v'] :: [='S',='S','S'].

startCategory('S').
