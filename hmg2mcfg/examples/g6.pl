%   File   : g6.pl
%   Author : E Stabler
%   Updated: 4 Nov 1998
%   Purpose: simple Kayne-ian treatment of relative clauses, from Utrecht slides

[]::[=t,c].             []::[=t,+wh_rel,c_rel].           []::[=e,+case,t].

[the]::[=n,d,-case].    [which]::[=n,+f,d,-case,-wh_rel].
                        [the]::[=c_rel,d,-case].
[hammer]::[n].          [hammer]::[n,-f].
[window]::[n].          [window]::[n,-f].

[]::[=v,+case,case].    []::[=case,+tr,tr].
[]::[=tr,=d,e].         []::[=v,=d,e].

[broke]::[=d,v,-tr].
[fell]::[v].

startCategory(c).
