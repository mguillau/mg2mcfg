%   File   : g22.pl
%   Author : E Stabler
%   Updated: Mar 00

['-s'] :: [v==>,+k,'T'].	% add just this for affix hopping

[] :: [='T','C'].              [] :: [=>'T','C'].            [] :: [=>'T',+wh,'C'].
['-s'] :: [=>'Modal',+k,'T'].  ['-s'] :: [=>'Have',+k,'T'].
['-s'] :: [=>'Be',+k,'T'].     ['-s'] :: [=v,+k,'T'].
[will] :: [='Have','Modal'].   [will] :: [='Be','Modal'].    [will] :: [=v,'Modal'].
[have] :: [='Been','Have'].    [have] :: [='En','Have'].
[be] :: [='Ing','Be'].           [been] :: [='Ing','Been'].
[] :: [=>'V',='D',v].          ['-en'] :: [=>'V',='D','En'].   ['-ing'] :: [=>'V',='D','Ing'].
[eat] :: [='D',+k,'V'].        [eat] :: ['V'].               [laugh] :: ['V'].
[the] :: [='N','D',-k].        [which] :: [='N','D',-k,-wh].
[king] :: ['N'].               [pie] :: ['N'].

[praise] :: [='D',+k,'V'].     [some] :: [='N','D',-k].

[have] :: [='Been','Have'].    [have] :: [='En','Have'].

['-e'] :: [=>v,+k,'T'].       ['-a'] :: [=>'Have',+k,'T'].
%[haber] :: [='En','Have'].    ['-ido'] :: [=>'V',='D','En'].
[haber] :: [='Ido','Have'].    ['-ido'] :: [=>'V',='D','Ido'].
[comer] :: [='D',+k,'V'].     [comer] :: ['V'].        [reir] :: ['V'].
[el] :: [='N','D',-k].        [cual] :: [='N','D',-k,-wh].
[rey] :: ['N'].               [pastel] :: ['N'].

%['-'] :: [=>'Have',+k,'T'].    ['-'] :: [=>v,+k,'T'].
%[haber] :: [='En','Have'].   ['-ido'] :: [=>'V',='D','En'].
%[comer] :: [='D',+k,'V'].      [reir] :: ['V'].
%[el] :: [='N','D',-k].         [cual] :: [='N','D',-k,-wh].
%[rey] :: ['N'].                [pastel] :: ['N'].

startCategory('C').
