%   File   : g4r.pl
%   Author : E Stabler
%   Updated: Mar 00
%   grammar for a rigid language with crossing dependencies

[] :: ['T'].
[x] :: ['T',-r,-l].           [y] :: [='T',+r,+l,'T'].
[c] :: [='T',+r,'A',-r].      [d] :: [='T',+r,'B',-r].
[a] :: [='A',+l,'T',-l].      [b] :: [='B',+l,'T',-l].

startCategory('T').
