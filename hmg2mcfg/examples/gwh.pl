%   File   : gwh.pl
%   Author : E Stabler
%   Updated: June 01

:- op(500, xfy, ::). % lexical items
:- op(500, fx, =). % for selection features

[]::[='T','C'].         []::[='T',+wh,'C'].
[]::[='V',='D','T'].
['Titus']::['D']. ['Tamora']::['D']. [who]::['D',-wh]. [the]::[='N','D']. [which]::[='N','D',-wh]. 
[queen]::['N'].
[enslaved]::[='D','V'].

startCategory('C').
