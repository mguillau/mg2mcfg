% do any simple properties of these expressions predict chartsize?
%  if so, I haven't found it

% Just load and run: analyze/0
% to produce a tab-separated db

%x(Number,String,Type,Derivations,ChartLength)
%         Type 0 = subject
%         Type 1 = object
%         Type 2 = ind object
%         Type 3 = oblique
%         Type 4 = genitive subject
%         Type 5 = genitive object

% from this we compute
%	string length
%	no of lex items in each derivation
%	no of clauses (CP's) in each derivation
%	no of selection,phrasal movement,head movement features in each derivation (sum = no of steps in derivation)

% operator defs - don't touch these
:- op(500, xfy, ::). % lexical items
:- op(500, fx, =). % for selection features
:- op(500, xf, <=). % for right incorporation
:- op(500, fx, =>). % for left incorporation
:- op(500, xf, ==>). % for right affix hop
:- op(500, fx, <==). % for left affix hop
:- op(500, xfy, <<). % for adjunction
:- op(500, xfy, >>). % for adjunction

% needed for no_of_movements
:- [larsonian,'../parser/mghapx','../parser/lhapx'].

analyze :-
	setof([Ch,StringLength,DerLengths,No,MvNo,Ty,N],
	      L^D^StringLength^DerLengths^(
		  x(N,L,Ty,D,Ch),
		  length(L,StringLength),
		  lengths(D,DerLengths),
		  no_of_clauses_in_ders(D,No),
		  no_of_Fs_in_ders(D,MvNo)
				     ),Analyses),
	Labels=[chartSize,stringLength,noOfLexItems,noOfClauses,noOfFs([sel,phrase,head]),type,exampleNo],
	writeAnalyses([Labels|Analyses]).

no_of_clauses_in_ders([D|Ds],[DNo|DNos]) :-
	no_of_clauses(D,DNo),
	no_of_clauses_in_ders(Ds,DNos).
no_of_clauses_in_ders([],[]).

lengths([D|Ds],[DNo|DNos]) :-
	length(D,DNo),
	lengths(Ds,DNos).
lengths([],[]).

% the lexical items of category C are: 0,1,2
no_of_clauses([0|Lexes],No) :- !,
	no_of_clauses(Lexes,No0),
	No is No0+1.
no_of_clauses([1|Lexes],No) :- !,
	no_of_clauses(Lexes,No0),
	No is No0+1.
no_of_clauses([2|Lexes],No) :- !,
	no_of_clauses(Lexes,No0),
	No is No0+1.
no_of_clauses([_|Lexes],No) :-
	no_of_clauses(Lexes,No).
no_of_clauses([],0).

no_of_Fs_in_ders([D|Ds],[[SelNo,DNo,HDNo]|DNos]) :-
	no_of_Fs(D,SelNo,DNo,HDNo),
	no_of_Fs_in_ders(Ds,DNos), !.
no_of_Fs_in_ders([],[]).

no_of_Fs(D,SelNo,MvNo,HMvNo) :- !,
	numlex(NumLexicon),listnum(D,NumLexicon,LexItems),
	no_of_triggers_in_items(LexItems,SelNo,MvNo,HMvNo).

no_of_triggers_in_items([],0,0,0).
no_of_triggers_in_items([(_String::Fs)|Is],Sel,No,HNo) :- !,
	no_of_triggers_in_list(Fs,ISel,INo,HINo),
	no_of_triggers_in_items(Is,IsSel,IsNo,HIsNo),
	Sel is ISel+IsSel,
	No is INo+IsNo,
	HNo is HINo+HIsNo.
no_of_triggers_in_items([_AdjunctionSymbol|Is],Sel,No,HNo) :- 
	no_of_triggers_in_items(Is,Sel,No,HNo).

% count affix hop as a kind of head movement
no_of_triggers_in_list([],0,0,0).
no_of_triggers_in_list([+_|L],S,N,H) :- !, no_of_triggers_in_list(L,S,LN,H), N is LN+1.
no_of_triggers_in_list([=>_|L],S,N,H) :- no_of_triggers_in_list(L,S,N,LHN), H is LHN+1. %left inc
no_of_triggers_in_list([_<=|L],S,N,H) :- no_of_triggers_in_list(L,S,N,LHN), H is LHN+1. %right inc
no_of_triggers_in_list([<==_|L],S,N,H) :- no_of_triggers_in_list(L,S,N,LHN), H is LHN+1. %left hop
no_of_triggers_in_list([_==>|L],S,N,H) :- no_of_triggers_in_list(L,S,N,LHN), H is LHN+1. %right hop
no_of_triggers_in_list([=_|L],S,N,H) :- no_of_triggers_in_list(L,LS,N,H), S is LS+1. %left inc
no_of_triggers_in_list([_|L],S,N,H) :- no_of_triggers_in_list(L,S,N,H).

writeAnalyses([]) :- nl.
writeAnalyses([Analysis|Analyses]) :- writeAnalysis(Analysis), writeAnalyses(Analyses).

writeAnalysis([]) :- nl.
writeAnalysis([A|T]) :- write(A), put(9), writeAnalysis(T).

%x(Number,String,Type,Derivations,ChartLength)
%type: (*subject*)
x(	1,
	[they,have,'-ed',forget,'-en',that,the,boy,who,tell,'-ed',the,story,be,'-s',so,young],
	subj,
	[[0,168,154,159,148,2,166,174,175,'>>',197,177,3,1,162,157,130,4,8,39,38,8,42,32]],
	13796
	).
x(	2,
	[the,fact,that,the,girl,who,pay,'-ed',for,the,ticket,'-s',be,'-s',very,poor,doesnt,matter],
	subj,
	[[0,173,157,127,4,8,109,2,166,174,175,'>>',198,178,3,1,162,157,144,120,112,4,9,69,38,8,44]],
	6466
	).
x(	3,
	['I',know,that,the,girl,who,get,'-ed',the,right,answer,be,'-s',clever],
	subj,
	[[0,163,157,147,2,166,174,175,179,3,1,162,157,135,4,8,'>>',193,75,38,8,44,33]],
	1833
	).
x(	4,
	[he,remember,'-ed',that,the,man,who,sell,'-ed',the,house,leave,'-ed',the,town],
	subj,
	[[0,162,157,149,2,162,157,136,4,8,57,3,1,162,157,134,4,8,55,38,8,46,31]],
	26262
	).
%type: (*direct object*)
x(	5,
	[they,have,'-ed',forget,'-en',that,the,letter,which,'Dick',write,'-ed',yesterday,be,'-s',long],
	obj,
	[[0,168,154,159,148,2,166,174,175,196,3,1,162,157,'<<',201,133,37,8,72,12,32]],
	1587
	).
x(	6,
	[the,fact,that,the,cat,which,'David',show,'-ed',to,the,man,like,'-s',eggs,be,'-s',strange],
	obj,
	[[0,166,174,175,194,4,8,109,2,161,157,137,27,3,1,162,157,123,116,110,4,8,45,37,8,52,13]],
	35840
	).
x(	7,
	['I',know,that,the,dog,which,'Penny',buy,'-ed',today,be,'-s',very,gentle],
	obj,
	[[0,163,157,147,2,166,174,175,'>>',198,180,3,1,162,157,'<<',202,138,37,8,50,14,33]],
	1086
	).
x(	8,
	[he,remember,'-ed',that,the,sweet,'-s',which,'David',give,'-ed','Sally',be,'-ed',a,treat],
	obj,
	[[0,162,157,149,2,169,174,176,5,8,61,3,1,162,158,122,116,115,15,37,9,66,13,31],
	 [0,162,157,149,2,169,174,176,5,8,61,3,1,162,158,122,116,115,37,9,66,15,13,31],
	 [0,162,158,149,2,169,174,176,5,8,61,3,1,162,157,122,116,115,37,9,66,15,13,31]
	 ],
	138986
	).
%type: (*indirect object*)
x(	9,
	[they,have,'-ed',forget,'-en',that,the,man,who,'Ann',give,'-ed',the,present,to,be,'-ed',old],
	iobj,
	[[0,168,154,159,148,2,169,174,175,195,3,1,162,157,122,116,110,38,8,46,4,8,67,26,32]],
	23056
	).
x(	10,
	[the,fact,that,the,boy,who,'Paul',sell,'-ed',the,book,to,hate,'-s',reading,be,'-s',strange],
	iobj,
	[[0,166,174,175,194,4,8,109,2,161,157,132,30,3,1,162,157,126,116,110,38,8,42,4,8,53,16]],
	72408
	).
x(	11,
	['I',know,that,the,man,who,'Stephen',explain,'-ed',the,accident,to,be,'-s',kind],
	iobj,
	[[0,163,157,147,2,166,174,175,181,3,1,162,157,124,116,110,38,8,46,4,8,77,17,33]],
	18094
	).
x(	12,
	[he,remember,'-ed',that,the,dog,which,'Mary',teach,'-ed',the,trick,to,be,'-s',clever],
	iobj,
	[[0,162,157,149,2,166,174,175,179,3,1,162,157,125,116,110,37,8,50,4,8,59,18,31]],
	18336
	).
%type: (*oblique*)
x(	13,
	[they,have,'-ed',forget,'-en',that,the,box,which,'Pat',bring,'-ed',the,apple,'-s',in,be,'-ed',lost],
	obl,
	[[0,168,154,159,148,2,169,174,175,183,3,1,162,157,'<<',117,114,37,8,80,139,4,9,81,19,32]],
	5237
	).
x(	14,
	[the,fact,that,the,girl,who,'Sue',write,'-ed',the,story,with,be,'-s',proud,doesnt,matter],
	obl,
	[[0,173,157,127,4,8,109,2,166,174,175,182,3,1,162,157,'<<',118,113,38,8,44,133,4,8,39,20]],
	3029
	).
x(	15,
	['I',know,that,the,ship,which,my,uncle,take,'-ed','Joe',on,be,'-ed',interesting],
	obl,
	[[0,163,157,147,2,169,174,175,185,3,1,162,157,'<<',119,111,37,8,88,140,21,6,8,93,33]],
	1923
	).
x(	16,
	[he,remember,'-ed',that,the,food,which,'Chris',pay,'-ed',the,bill,for,be,'-ed',cheap],
	obl,
	[[0,162,157,149,2,169,174,175,184,3,1,162,157,'<<',120,112,37,8,84,145,4,8,103,11,31]],
	2077
	).
%type: (*genitive subject*)
x(	17,
	[they,have,'-ed',forget,'-en',that,the,girl,who,s,friend,buy,'-ed',the,cake,be,'-ed',wait,'-ing'],
	gensubj,
	[[0,168,154,159,148,2,169,155,160,128,3,1,162,157,138,4,8,85,38,8,36,8,101,8,44,32]],
	3750
	).
x(	18,
	[the,fact,that,the,boy,who,s,brother,tell,'-s',lies,be,'-s',always,honest,surprise,'-ed',us],
	gensubj,
	[[0,162,157,143,35,4,8,109,2,166,174,175,'>>',199,192,3,1,161,157,130,29,38,8,36,8,99,8,42]],
	40150
	).
x(	19,
	['I',know,that,the,boy,who,s,father,sell,'-ed',the,dog,be,'-ed',very,sad],
	gensubj,
	[[0,163,157,147,2,169,174,175,'>>',198,186,3,1,162,157,134,4,8,49,38,8,36,8,97,8,42,33]],
	11802
	).
x(	20,
	[he,remember,'-ed',that,the,girl,who,s,mother,send,'-ed',the,clothe,'-s',come,'-ed',too,late],
	gensubj,
	[[0,162,157,149,2,162,157,146,'>>',200,187,3,1,162,157,141,4,9,107,38,8,36,8,95,8,44,31]],
	2653
	).
%type: (*genitive object*)
x(	21,
	[they,have,'-ed',forget,'-en',that,the,man,who,s,house,'Patrick',buy,'-ed',be,'-ed',so,ill],
	genobj,
	[[0,168,154,159,148,2,169,174,175,'>>',197,188,3,1,162,157,138,38,8,36,8,55,8,46,22,32]],
	2039
	).
x(	22,
	[the,fact,that,the,sailor,who,s,ship,'Jim',take,'-ed',have,'-ed',one,leg,be,'-s',important],
	genobj,
	[[0,166,174,175,189,4,8,109,2,162,157,142,7,8,105,3,1,162,157,140,38,8,36,8,87,8,92,23]],
	7441
	).
x(	23,
	['I',know,that,the,woman,who,s,car,'Jenny',sell,'-ed',be,'-ed',very,angry],
	genobj,
	[[0,163,157,147,2,169,174,175,'>>',198,190,3,1,162,157,134,38,8,36,8,89,8,48,24,33]],
	11802
	).
x(	24,
	[he,remember,'-ed',that,the,girl,who,s,picture,'Clare',show,'-ed',us,be,'-ed',pretty],
	genobj,
	[[0,162,157,149,2,169,174,175,191,3,1,162,158,123,116,115,35,38,8,36,8,73,8,44,25,31],
	 [0,162,157,149,2,169,174,175,191,3,1,162,158,123,116,115,38,8,36,8,73,8,44,35,25,31],
	 [0,162,158,149,2,169,174,175,191,3,1,162,157,123,116,115,38,8,36,8,73,8,44,35,25,31]],
	10144
	).
