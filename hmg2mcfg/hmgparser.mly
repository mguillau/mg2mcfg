%{
(** Parses the stream returned by the Hmglexer lexer according to the grammar of hMGs defined in hmgparser.mly.
    Please note that your grammar should start with a line defining its start symbols.
	@author Matthieu Guillaumin
	@see 'hmgparser.mly' for the explicit accepted grammar *)
open Hmgtransform.Feature;;
open Hmgtransform.Rule;;
open Hmgtransform.Adjunction;;
open Hmgtransform.LexItem;;
open Hmgverify;;

%}

%token ENDRULE ENDGRAMMAR
%token DEFINE
%token EQUAL PLUS MINUS GREATER SMALLER
%token EMPTY

%token <string> IDENT

%start mg
%type <string list * Hmgtransform.Rule.grammar * Hmgtransform.Adjunction.t list> mg

%%

mg:
  startlist lexrulelist ENDGRAMMAR {check($1,$2)}
;

startlist:
  IDENT slist ENDRULE {$1::$2}
;

slist:
 {[]}
| IDENT slist {$1::$2}
;

lexrulelist:
 {[]}
| lexrule lexrulelist {$1::$2}
;

lexrule:
 vocab sign featurelist category licenseelist ENDRULE { Rule {voc=$1; nature=LEXICAL; features=($3@[$4]@$5)} }
| category licenseelist adjunction category licenseelist ENDRULE { Adjunction {side=$3; leftcat=$1; left=$2; rightcat=$4; right=$5} }
;

sign:
  DEFINE {LEXICAL}
;

adjunction:
  GREATER GREATER {LEFT}
| SMALLER SMALLER {RIGHT}
;

vocab:
  {""}
| IDENT {$1}
;

featurelist:
 {[]}
| feat featurelist {$1::$2}
;

feat:
 featmod IDENT { {name=$2; modifier=$1} }
;

featmod:
  EQUAL {SELECTOR}
| PLUS  {LICENSOR}
| SMALLER EQUAL EQUAL {AFFIXHOP_L}
| SMALLER EQUAL {HEADMV_R}
| EQUAL GREATER {HEADMV_L}
| EQUAL EQUAL GREATER {AFFIXHOP_R}
;

category:
  IDENT { {name=$1; modifier=CATEGORY} }
;

licenseelist:
  {[]}
| licensee licenseelist {$1::$2}
;

licensee:
| MINUS IDENT { {name=$2; modifier=LICENSEE} }
;
%%
