{
(** Returns a stream of lexemes defined in hmglexer_pl.mll
	@author Matthieu Guillaumin
	@see 'hmglexer_pl.mll' for lexemes used *)
open Hmgparser_pl;;
exception LexingError of int*string
}

rule token_pl = parse
 [' ' '\t' '\n' '\r'] {token_pl lexbuf}
| ( '%' [^'\n']* '\n' )   {token_pl lexbuf}   (* everything behind a % is ignored *)
| '.' {ENDRULE}
| "::" {DEFINE}
| '=' {EQUAL}
| '+' {PLUS}
| '-' {MINUS}
| '>' {GREATER}
| '<' {SMALLER}
| ',' {COMA}
| ')' {RIGHTPAR}
| '[' {LEFT}
| ']' {RIGHT}
| "startCategory(" {START}
| (['a'-'z' '_' 'A'-'Z' '0'-'9'])+ {IDENT(Lexing.lexeme lexbuf)}
| ('\"' ([^'\"']* as idiom) '\"') {IDENT(idiom)}
| ('\'' ([^'\'']* as idiom) '\'') {IDENT(idiom)}
| eof {ENDGRAMMAR}
| (_) {raise(LexingError(Lexing.lexeme_start lexbuf,Lexing.lexeme lexbuf))}
